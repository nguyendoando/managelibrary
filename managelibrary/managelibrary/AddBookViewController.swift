//
//  AddBookViewController.swift
//  managelibrary
//
//  Created by Nguyen Do on 5/17/17.
//
//

import UIKit
import CoreData

class AddBookViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var lblMajors: UILabel!
    @IBOutlet weak var txtIdBook: UITextField!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtAuthor: UITextField!
    @IBOutlet weak var txtNumber: UITextField!
    @IBOutlet weak var txtBookself: UITextField!
    @IBOutlet weak var txtPrice: UITextField!
    @IBOutlet weak var txtRentedPrice: UITextField!
    @IBOutlet weak var imgBook: UIImageView!
    @IBOutlet weak var txtDescript: UITextView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    var majors: Majors!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        addDoneButtonTextField(to: txtIdBook)
        addDoneButtonTextField(to: txtName)
        addDoneButtonTextField(to: txtAuthor)
        addDoneButtonTextField(to: txtNumber)
        addDoneButtonTextField(to: txtBookself)
        addDoneButtonTextField(to: txtPrice)
        addDoneButtonTextField(to: txtRentedPrice)
        addDoneButtonTextView(to: txtDescript)
        
        lblMajors.layer.cornerRadius = 6
        lblMajors.layer.masksToBounds = true
        // Do any additional setup after loading the view.
        imgBook.isUserInteractionEnabled = true
        let tapgetsture = UITapGestureRecognizer(target: self, action: #selector(ImageTapped(sender:)))
        self.imgBook.addGestureRecognizer(tapgetsture)
        
        lblMajors.text = majors.name
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: .UIKeyboardWillHide, object: nil)
    }
    
    func keyboardWillShow(_ notification: Notification){
        scrollView.isScrollEnabled = true
        
        let info = notification.userInfo
        let keyboard = (info?[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue.size
        
        let contentInsets = UIEdgeInsetsMake(0, 0, keyboard.height, 0)
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
    }
    
    func keyboardWillHide(_ notification: Notification){
        let info = notification.userInfo
        let keyboard = (info?[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue.size
        
        let contentInsets = UIEdgeInsetsMake(0, 0, -keyboard.height, 0)
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
        
        self.scrollView.isScrollEnabled = false
    }
    
    func ImageTapped(sender: AnyObject){
        print("Image tapped!")
        let image = UIImagePickerController()
        image.delegate = self
        image.allowsEditing = true
        
        present(image, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let image = info[UIImagePickerControllerOriginalImage] as? UIImage
        imgBook.image = image
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func checkInfo() -> Bool{
        let code = txtIdBook.text!
        if code.isEmpty{
            alertWarnning(title: "Thông báo", message: "Mã sách không được để trống.")
            return false
        }
        var book = [Book]()
        let fetchRequest:NSFetchRequest<Book> = Book.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "idbook == %@", code)
        do{
            let searchResults = try Database.getContext().fetch(fetchRequest)
            book = searchResults
            if book.count > 0{
                alertWarnning(title: "Thông báo", message: "Mã sách này đã tồn tại.")
                return false
            }
        }
        catch{
            print("Error: \(error)")
        }
        
        let name = txtName.text!
        if name.isEmpty{
            alertWarnning(title: "Thông báo", message: "Tên sách không được để trống.")
            return false
        }
        let author = txtAuthor.text!
        if author.isEmpty{
            alertWarnning(title: "Thông báo", message: "Tên tác giả không được để trống.")
            return false
        }
        let id = txtNumber.text!
        let num = Int(id)
        if num != nil{
            if num! <= 0 {
                alertWarnning(title: "Thông báo", message: "Số lượng sách phải lớn hơn 0.")
                return false
            }
        }else{
            alertWarnning(title: "Thông báo", message: "Số lượng bắt buộc phải nhập vào số nguyên.")
            return false
        }
        let bookself = txtBookself.text!
        let numbs = Int(bookself)
        if numbs != nil{
            if numbs! <= 0{
                alertWarnning(title: "Thông báo", message: "Vị trí kệ sách phải lớn hơn 0.")
                return false
            }
        }else{
            alertWarnning(title: "Thông báo", message: "Vị trí kệ sách bắt buộc phải nhập vào số nguyên.")
            return false
        }
        let price = txtPrice.text!
        let prices = Double(price)
        if prices != nil{
            if prices! <= 0{
                alertWarnning(title: "Thông báo", message: "Giá sách phải lớn hơn 0.")
                return false
            }
        }else{
            alertWarnning(title: "Thông báo", message: "Giá sách bắt buộc phải nhập vào số.")
            return false
        }
        let rent = txtRentedPrice.text!
        let rents = Double(rent)
        if rents != nil{
            if rents! <= 0{
                alertWarnning(title: "Thông báo", message: "Giá mượn sách phải lớn hơn 0.")
                return false
            }
        }else{
            alertWarnning(title: "Thông báo", message: "Giá mượn sách bắt buộc phải nhập vào số.")
            return false
        }
        let descript = txtDescript.text!
        if descript.isEmpty{
            alertWarnning(title: "Thông báo", message: "Bạn cần phải mô tả thông tin sách này.")
            return false
        }
        return true
    }
    
    @IBAction func addBook(_ sender: Any) {
        let isSuccess = checkInfo()
        if isSuccess == false{
            return
        }
        let bookClassName:String = String(describing: Book.self)
        let book:Book = NSEntityDescription.insertNewObject(forEntityName: bookClassName, into: Database.getContext()) as! Book
        
        book.idbook = txtIdBook.text!
        book.name = txtName.text!
        book.author = txtAuthor.text!
        book.number = Int32(txtNumber.text!)!
        book.restnumber = Int32(txtNumber.text!)!
        book.bookself = Int32(txtBookself.text!)!
        book.price = Double(txtPrice.text!)!
        book.rentedprice = Double(txtRentedPrice.text!)!
        book.descript = txtDescript.text!
        let imgData = UIImageJPEGRepresentation(imgBook.image!, 1)
        book.setValue(imgData, forKey: "image")
        
        book.majors = majors
        
        Database.saveContext()
        print("Insert successfully!")
        
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
