//
//  BookArea+CoreDataProperties.swift
//  managelibrary
//
//  Created by cao cuong on 6/4/17.
//
//

import Foundation
import CoreData


extension BookArea {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<BookArea> {
        return NSFetchRequest<BookArea>(entityName: "BookArea");
    }

    @NSManaged public var descript: String?
    @NSManaged public var image: NSData?
    @NSManaged public var name: String?
    @NSManaged public var majors: NSSet?

}

// MARK: Generated accessors for majors
extension BookArea {

    @objc(addMajorsObject:)
    @NSManaged public func addToMajors(_ value: Majors)

    @objc(removeMajorsObject:)
    @NSManaged public func removeFromMajors(_ value: Majors)

    @objc(addMajors:)
    @NSManaged public func addToMajors(_ values: NSSet)

    @objc(removeMajors:)
    @NSManaged public func removeFromMajors(_ values: NSSet)

}
