//
//  BookInfo.swift
//  managelibrary
//
//  Created by Nguyen Do on 5/18/17.
//
//

import UIKit
class BookInfo : NSObject{

    var id = 0
    var lblNameBook = ""
    var lblAuthor = ""
    var lblNumberCode:Int = 0
    var tvDescription = ""
    var lblNumberCount:Int = 0
    var lblBookShelf = ""
    var imgImageBook: UIImage = UIImage(named: "book2.jpg")!
}
