//
//  AddEditAreaViewController.swift
//  managelibrary
//
//  Created by cao cuong on 5/25/17.
//
//

import UIKit
import CoreData

class AddEditAreaViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var imgArea: UIImageView!
    @IBOutlet weak var txtDescript: UITextView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var btnEdit: UIButton!
    
    var isEdit = false
    var pos = -1
    var bookarea = [BookArea]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addDoneButtonTextField(to: txtName)
        addDoneButtonTextView(to: txtDescript)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: .UIKeyboardWillHide, object: nil)
        // Do any additional setup after loading the view.
        imgArea.isUserInteractionEnabled = true
        let tapgetsture = UITapGestureRecognizer(target: self, action: #selector(ImageTapped(sender:)))
        self.imgArea.addGestureRecognizer(tapgetsture)
        
        if isEdit{
            btnEdit.setTitle("Sửa", for: .normal)
            txtName.text = bookarea[pos].name
            txtDescript.text = bookarea[pos].descript
            let image : UIImage = UIImage(data: bookarea[pos].image as! Data)!
            imgArea.image = image
        }
    }
    
    func keyboardWillShow(_ notification: Notification){
        scrollView.isScrollEnabled = true
        
        let info = notification.userInfo
        let keyboard = (info?[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue.size
        
        let contentInsets = UIEdgeInsetsMake(0, 0, keyboard.height, 0)
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
    }
    
    func keyboardWillHide(_ notification: Notification){
        let info = notification.userInfo
        let keyboard = (info?[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue.size
        
        let contentInsets = UIEdgeInsetsMake(0, 0, -keyboard.height, 0)
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
        
        self.scrollView.isScrollEnabled = false
    }
    
    func ImageTapped(sender: AnyObject){
        print("Image tapped!")
        let image = UIImagePickerController()
        image.delegate = self
        image.allowsEditing = true
        
        present(image, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let image = info[UIImagePickerControllerOriginalImage] as? UIImage
        imgArea.image = image
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func checkInfo() -> Bool{
        let name = txtName.text!
        if name.isEmpty{
            alertWarnning(title: "Thông báo", message: "Tên khu vực không được để trống")
            return false
        }
        let descript = txtDescript.text!
        if descript.isEmpty{
            alertWarnning(title: "Thông báo", message: "Bạn cần phải mô tả thông tin khu vực này")
            return false
        }
        return true
    }
    
    @IBAction func addArea(_ sender: Any) {
        let isSuccess = checkInfo()
        if isSuccess == false{
            return
        }
        
        let areaClassName:String = String(describing: BookArea.self)
        
        if isEdit{
            bookarea[pos].name = txtName.text!
            bookarea[pos].descript = txtDescript.text!
            let imgData = UIImageJPEGRepresentation(imgArea.image!, 1)
            bookarea[pos].image = imgData as NSData?
            
            Database.saveContext()
            print("Update successfully!")
        }
        else{
            let bookArea:BookArea = NSEntityDescription.insertNewObject(forEntityName: areaClassName, into: Database.getContext()) as! BookArea
            
            bookArea.name = txtName.text!
            bookArea.descript = txtDescript.text!
            let imgData = UIImageJPEGRepresentation(imgArea.image!, 1)
            bookArea.setValue(imgData, forKey: "image")
            
            Database.saveContext()
            print("Insert successfully!")
        }
        txtName.text = nil
        txtDescript.text = nil
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    
}
