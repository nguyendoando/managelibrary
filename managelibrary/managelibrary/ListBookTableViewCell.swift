//
//  ListBookTableViewCell.swift
//  managelibrary
//
//  Created by Nguyen Do on 5/15/17.
//
//

import UIKit

class ListBookTableViewCell: UITableViewCell {

    @IBOutlet weak var imgListBook: UIImageView!
    @IBOutlet weak var lblAreaBook: UILabel!
    @IBOutlet weak var lblSpecialized: UILabel!
    @IBOutlet weak var btnAddBook: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        if(GlobaVariable.global == false){
            btnAddBook.isHidden = true
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
