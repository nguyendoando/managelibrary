//
//  NoticationsBookTableViewCell.swift
//  managelibrary
//
//  Created by Nguyen Do on 5/17/17.
//
//

import UIKit

class NoticationsBookTableViewCell: UITableViewCell {

    
    @IBOutlet weak var lblMSSV: UILabel!
    @IBOutlet weak var lblBookCode: UILabel!
    @IBOutlet weak var lblDayBorrow: UILabel!
    @IBOutlet weak var lblSend: UILabel!
    @IBOutlet weak var lblNumber: UILabel!
    @IBOutlet weak var lblNumberDayEnd: UILabel!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
