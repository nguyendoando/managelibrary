//
//  BorrowedDetail+CoreDataProperties.swift
//  managelibrary
//
//  Created by cao cuong on 6/4/17.
//
//

import Foundation
import CoreData


extension BorrowedDetail {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<BorrowedDetail> {
        return NSFetchRequest<BorrowedDetail>(entityName: "BorrowedDetail");
    }

    @NSManaged public var number: Int32
    @NSManaged public var borroweddate: NSDate?
    @NSManaged public var returneddate: NSDate?
    @NSManaged public var book: Book?
    @NSManaged public var card: LibraryCard?

}
