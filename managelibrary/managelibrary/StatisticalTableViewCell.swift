//
//  StatisticalTableViewCell.swift
//  managelibrary
//
//  Created by Nguyen Do on 5/17/17.
//
//

import UIKit

class StatisticalTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imgBook: UIImageView!
    @IBOutlet weak var lblBookName: UILabel!
    @IBOutlet weak var lblNumber: UILabel!
    @IBOutlet weak var lblTotal: UILabel!
    @IBOutlet weak var lblBorrowedDate: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
