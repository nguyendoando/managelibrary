//
//  SignupViewController.swift
//  managelibrary
//
//  Created by Nguyen Do on 5/17/17.
//
//

import UIKit
import CoreData
class SignupViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var btnMenu: UIBarButtonItem!
    @IBOutlet weak var btnSignup: UIButton!
    @IBOutlet weak var imgAccount: UIImageView!
    @IBOutlet weak var txtNameAccount: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtCorrectPassword: UITextField!
    @IBOutlet weak var scrollView: UIScrollView!
    
    var listAccount = [Account]()
    var SignupAccount = [Account]()
    var pos = -1
    override func viewDidLoad() {
        super.viewDidLoad()
        btnSignup.layer.masksToBounds = true
        btnSignup.layer.cornerRadius = 8
        
        if revealViewController() != nil {
            btnMenu.target = revealViewController()
            btnMenu.action = #selector(SWRevealViewController.revealToggle(_:))
        }
        
        addDoneButtonTextField(to: txtNameAccount)
        addDoneButtonTextField(to: txtEmail)
        addDoneButtonTextField(to: txtPassword)
        addDoneButtonTextField(to: txtCorrectPassword)
        
        txtPassword.isSecureTextEntry = true
        txtCorrectPassword.isSecureTextEntry = true
        // Do any additional setup after loading the view.
        imgAccount.isUserInteractionEnabled = true
        let tapgetsture = UITapGestureRecognizer(target: self, action: #selector(ImageTapped(sender:)))
        self.imgAccount.addGestureRecognizer(tapgetsture)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: .UIKeyboardWillHide, object: nil)
    }
    
    func keyboardWillShow(_ notification: Notification){
        scrollView.isScrollEnabled = true
        
        let info = notification.userInfo
        let keyboard = (info?[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue.size
        
        let contentInsets = UIEdgeInsetsMake(0, 0, keyboard.height, 0)
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
    }
    
    func keyboardWillHide(_ notification: Notification){
        let info = notification.userInfo
        let keyboard = (info?[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue.size
        
        let contentInsets = UIEdgeInsetsMake(0, 0, -keyboard.height, 0)
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
        
        self.scrollView.isScrollEnabled = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getAccount()
    }
    func ImageTapped(sender: AnyObject){
        print("Image tapped!")
        let image = UIImagePickerController()
        image.delegate = self
        image.allowsEditing = true
        
        present(image, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let image = info[UIImagePickerControllerOriginalImage] as? UIImage
        self.imgAccount.image = image
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func checkInfo() -> Bool{
        let name = txtNameAccount.text!
        if name.isEmpty{
            alertWarnning(title: "Thông báo", message: "Tên tài khoản không được để trống")
            return false
        }
        let email =  txtEmail.text!
        if email.isEmpty{
            alertWarnning(title: "Thông báo", message: "Email không được để trống")
            return false
        }
        let xfEmail = comfirmEmail(Email: email)
        if(xfEmail == false){
            alertWarnning(title: "Thông báo", message: "Email không tồn tại")
            return false
        }
        
            //***** Comfirm email******
            let ix = SignupAccount.count
            for i in 0 ..< ix {
                print("================\(SignupAccount[i].email)")
                if((email == SignupAccount[i].email)){
                    alertWarnning(title: "Thông báo", message: "Email này đã tồn tại")
                    return false
                }
            }
            //***************

        //**** Comfirm Password
        let password = txtPassword.text!
        if password.isEmpty{
            alertWarnning(title: "Thông báo", message: "Password không được để trống")
            return false
        }

        let i = password.characters.count
        if (i < 7){
            alertWarnning(title: "Thông báo", message: "Password phải lớn hơn 6 ký tự")
            return false
        }
        let bPass = validateCharacters(password: password)
        if(bPass == false){
            alertWarnning(title: "Thông báo", message: "Password phải có chữ hoa, chữ thường, số, ký tự đặc biệt")
            return false
        }
        //*****************
        let correctPassword = txtCorrectPassword.text!
        if correctPassword.isEmpty{
            alertWarnning(title: "Thông báo", message: "Password xác nhận không được để trống")
            return false
        }
        if(txtPassword.text != txtCorrectPassword.text){
            alertWarnning(title: "Thông báo", message: "Xác nhận mật khẩu không trùng khớp")
            return false
        }
        return true
    }
    let accountClassName:String = String(describing: Account.self)

    @IBAction func btnSignupAccount(_ sender: Any) {
        let isSuccess = checkInfo()
        if isSuccess == false{
            return
        }
        let account:Account = NSEntityDescription.insertNewObject(forEntityName: accountClassName, into: Database.getContext()) as! Account
        account.name = txtNameAccount.text!
        account.email = txtEmail.text!

        account.password = txtPassword.text!
        let imgData = UIImageJPEGRepresentation(imgAccount.image!, 1)
        account.image = imgData as NSData?
        Database.saveContext()
        //alertWarnning(title: "Thông báo", message: "Đăng ký thành công")
        txtNameAccount.text = nil
        txtEmail.text = nil
        txtPassword.text = nil
        txtCorrectPassword.text = nil
        //self.navigationController?.popViewController(animated: true)
        performSegue(withIdentifier: "detailAccount_segue", sender: self)
    }
    func getAccount(){
        SignupAccount.removeAll()
        let fetchRequest: NSFetchRequest<Account> = Account.fetchRequest()
        do{
            let searchResults = try Database.getContext().fetch(fetchRequest)
            SignupAccount = searchResults
        }
        catch{
            print("Error: \(error)")
        }
    }

}
