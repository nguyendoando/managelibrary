//
//  Book+CoreDataProperties.swift
//  managelibrary
//
//  Created by cao cuong on 6/12/17.
//
//

import Foundation
import CoreData


extension Book {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Book> {
        return NSFetchRequest<Book>(entityName: "Book");
    }

    @NSManaged public var author: String?
    @NSManaged public var bookself: Int32
    @NSManaged public var descript: String?
    @NSManaged public var idbook: String?
    @NSManaged public var image: NSData?
    @NSManaged public var name: String?
    @NSManaged public var number: Int32
    @NSManaged public var price: Double
    @NSManaged public var rentedprice: Double
    @NSManaged public var restnumber: Int32
    @NSManaged public var borrowed: NSSet?
    @NSManaged public var majors: Majors?
    @NSManaged public var bill: NSSet?

}

// MARK: Generated accessors for borrowed
extension Book {

    @objc(addBorrowedObject:)
    @NSManaged public func addToBorrowed(_ value: BorrowedDetail)

    @objc(removeBorrowedObject:)
    @NSManaged public func removeFromBorrowed(_ value: BorrowedDetail)

    @objc(addBorrowed:)
    @NSManaged public func addToBorrowed(_ values: NSSet)

    @objc(removeBorrowed:)
    @NSManaged public func removeFromBorrowed(_ values: NSSet)

}

// MARK: Generated accessors for bill
extension Book {

    @objc(addBillObject:)
    @NSManaged public func addToBill(_ value: Bill)

    @objc(removeBillObject:)
    @NSManaged public func removeFromBill(_ value: Bill)

    @objc(addBill:)
    @NSManaged public func addToBill(_ values: NSSet)

    @objc(removeBill:)
    @NSManaged public func removeFromBill(_ values: NSSet)

}
