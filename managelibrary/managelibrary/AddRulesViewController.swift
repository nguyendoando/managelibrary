//
//  AddRulesViewController.swift
//  managelibrary
//
//  Created by cao cuong on 5/29/17.
//
//

import UIKit
import CoreData

class AddRulesViewController: UIViewController {

    @IBOutlet weak var txtContent: UITextView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var btnEdit: UIButton!
    
    var rule: Rules!
    var bookReg: BookRegulations!
    var bookRule: BookRules!
    var isEdit = false
    var option = -1
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if isEdit{
            btnEdit.setTitle("Sửa", for: .normal)
            if option == 1{
                txtContent.text = rule.contents
            }
            if option == 2{
                txtContent.text = bookReg.contents
            }
            if option == 3{
                txtContent.text = bookRule.contents
            }
        }
        addDoneButtonTextView(to: txtContent)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: .UIKeyboardWillHide, object: nil)
    }

    func keyboardWillShow(_ notification: Notification){
        scrollView.isScrollEnabled = true
        
        let info = notification.userInfo
        let keyboard = (info?[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue.size
        
        let contentInsets = UIEdgeInsetsMake(0, 0, keyboard.height, 0)
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
    }
    
    func keyboardWillHide(_ notification: Notification){
        let info = notification.userInfo
        let keyboard = (info?[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue.size
        
        let contentInsets = UIEdgeInsetsMake(0, 0, -keyboard.height, 0)
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
        
        self.scrollView.isScrollEnabled = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func checkInfo() -> Bool{
        let content = txtContent.text!
        if content.isEmpty{
            alertWarnning(title: "Thông báo", message: "Nội dung không được để trống.")
            return false
        }
        return true
    }
    
    @IBAction func addRules(_ sender: Any) {
        let isSuccess = checkInfo()
        if isSuccess == false{
            return
        }
        if isEdit{
            if option == 1{
                rule.contents = txtContent.text!
                Database.saveContext()
                print("Update successfully!")
            }
            if option == 2{
                bookReg.contents = txtContent.text!
                Database.saveContext()
                print("Update successfully!")
            }
            if option == 3{
                bookRule.contents = txtContent.text!
                Database.saveContext()
                print("Update successfully!")
            }
        }
        else{
            if option == 1{
                let ruleClassName:String = String(describing: Rules.self)
                let rules:Rules = NSEntityDescription.insertNewObject(forEntityName: ruleClassName, into: Database.getContext()) as! Rules
                
                rules.contents = txtContent.text!
                
                Database.saveContext()
                print("Insert successfully!")
            }
            if option == 2{
                let bookRegClassName:String = String(describing: BookRegulations.self)
                let bookRe:BookRegulations = NSEntityDescription.insertNewObject(forEntityName: bookRegClassName, into: Database.getContext()) as! BookRegulations
                
                bookRe.contents = txtContent.text!
                
                Database.saveContext()
                print("Insert successfully!")
            }
            if option == 3{
                let bookRuleClassName:String = String(describing: BookRules.self)
                let bookRules:BookRules = NSEntityDescription.insertNewObject(forEntityName: bookRuleClassName, into: Database.getContext()) as! BookRules
                
                bookRules.contents = txtContent.text!
                
                Database.saveContext()
                print("Insert successfully!")
            }
        }
        
        _ = self.navigationController?.popViewController(animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
