//
//  BorrowedViewController.swift
//  managelibrary
//
//  Created by cao cuong on 6/4/17.
//
//

import UIKit
import CoreData

class BorrowedViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var dpkBorrowed: UIDatePicker!
    @IBOutlet weak var dpkExp: UIDatePicker!
    @IBOutlet weak var txtCardCode: UITextField!
    @IBOutlet weak var txtBookCode: UITextField!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var lblTotal: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!

    var bookData = [Book]()
    var borrowedCount = [Int32]()
    var rentedTotal = [Double]()
    var detail = [BorrowedDetail]()
    var card = [LibraryCard]()
    var isUnLock = true
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        addDoneButtonTextField(to: txtCardCode)
        addDoneButtonTextField(to: txtBookCode)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: .UIKeyboardWillHide, object: nil)
        
        dpkBorrowed.isEnabled = false
        self.tableView.dataSource = self
        self.tableView.delegate = self
    }

    func keyboardWillShow(_ notification: Notification){
        scrollView.isScrollEnabled = true
        
        let info = notification.userInfo
        let keyboard = (info?[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue.size
        
        let contentInsets = UIEdgeInsetsMake(0, 0, keyboard.height, 0)
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
    }
    
    func keyboardWillHide(_ notification: Notification){
        let info = notification.userInfo
        let keyboard = (info?[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue.size
        
        let contentInsets = UIEdgeInsetsMake(0, 0, -keyboard.height, 0)
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
        
        self.scrollView.isScrollEnabled = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return bookData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "ListBorrowedCell", for: indexPath) as! ListBorrowedTableViewCell
        
        // Configure the cell...
        cell.lblCode.text = bookData[indexPath.row].idbook
        cell.lblName.text = bookData[indexPath.row].name
        cell.lblNumber.text = String(borrowedCount[indexPath.row])
        
        return cell
    }
    
    // Override to support conditional editing of the table view.
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    
    // Override to support editing the table view.
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            bookData.remove(at: indexPath.row)
            borrowedCount.remove(at: indexPath.row)
            self.tableView.reloadData()
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }
    }
    
    @IBAction func addToListBook(_ sender: Any) {
        let cardCode = txtCardCode.text!
        if cardCode.isEmpty{
            alertWarnning(title: "Thông báo", message: "Mã thẻ không được để trống.")
            return
        }
        if isUnLock{
            getCardInfo(ma: cardCode)
            if card.count > 0{
                txtCardCode.isEnabled = false
                isUnLock = false
            }
            else{
                alertWarnning(title: "Thông báo", message: "Mã thẻ này không tồn tại.")
                return
            }
        }
        let bookCode = txtBookCode.text!
        if bookCode.isEmpty{
            alertWarnning(title: "Thông báo", message: "Mã sách không được để trống.")
            return
        }
        getBorrowedDetails(ma: cardCode)
        let number = detail.count
        var sum:Int32 = 0
        var borrowedTotal: Int32 = 0
        var isFlag = false
        if number > 0{
            for i in 0..<number{
                borrowedTotal += detail[i].number
                sum = borrowedTotal
                isFlag = true
            }
        }
        sum += sumNumber()
        sum += 1
        if sum > 10{
            if isFlag{
                let msg = "Bạn đã mượn " + String(borrowedTotal) + " cuốn sách chưa trả. Không được phép mượn quá 10 cuốn sách."
                alertWarnning(title: "Thông báo", message: msg)
            }
            else{
                alertWarnning(title: "Thông báo", message: "Không được phép mượn quá 10 cuốn sách.")
            }
            return
        }
        getBook(ma: bookCode)
        let total = totalPrice()
        lblTotal.text = String(total) + "đ"
        self.tableView.reloadData()
    }

    func totalPrice()->Double{
        var total = 0.0
        for i in 0..<bookData.count{
            total += rentedTotal[i] * Double(borrowedCount[i])
        }
        return total
    }
    
    func checkDate() -> Bool{
        let dateBorrowed = dpkBorrowed.date
        let calendar = Calendar.current
        
        let year = calendar.component(.year, from: dateBorrowed)
        let month = calendar.component(.month, from: dateBorrowed)
        let day = calendar.component(.day, from: dateBorrowed)
        
        let dateExp = dpkExp.date
        
        let yearExp = calendar.component(.year, from: dateExp)
        let monthExp = calendar.component(.month, from: dateExp)
        let dayExp = calendar.component(.day, from: dateExp)
        
        if yearExp < year{
            return false
        }
        if yearExp == year{
            if monthExp < month{
                return false
            }
            if monthExp == month{
                if dayExp <= day{
                    return false
                }
            }
        }
        return true
    }
    
    func sumNumber() -> Int32{
        let number = borrowedCount.count
        var s:Int32 = 0
        for i in 0..<number{
            s += borrowedCount[i]
        }
        return s
    }
    
    @IBAction func addToBorrowedDetail(_ sender: Any) {
        let cardCode = txtCardCode.text!
        if cardCode.isEmpty{
            alertWarnning(title: "Thông báo", message: "Mã thẻ không được để trống.")
            return
        }
        let check = checkDate()
        if check{
            if bookData.count > 0{
                let number = bookData.count
                for i in 0..<number{
                    let isUpdate = updateBorrowedNumber(ma: bookData[i].idbook!, maThe: card[0].code!, sl: borrowedCount[i])
                    if isUpdate == false{
                        let borrowedClassName:String = String(describing: BorrowedDetail.self)
                        let borrowed:BorrowedDetail = NSEntityDescription.insertNewObject(forEntityName: borrowedClassName, into: Database.getContext()) as! BorrowedDetail
                        
                        borrowed.card = card[0]
                        bookData[i].restnumber -= borrowedCount[i]
                        borrowed.book = bookData[i]
                        borrowed.number = borrowedCount[i]
                        borrowed.borroweddate = dpkBorrowed.date as NSDate?
                        borrowed.returneddate = dpkExp.date as NSDate?
                        
                        let billClassName:String = String(describing: Bill.self)
                        let bill:Bill = NSEntityDescription.insertNewObject(forEntityName: billClassName, into: Database.getContext()) as! Bill
                        
                        bill.billbook = bookData[i]
                        bill.paydate = dpkBorrowed.date as NSDate?
                        bill.number = borrowedCount[i]
                        bill.rentedprice = bookData[i].rentedprice
                        
                        Database.saveContext()
                        print("Insert successfully!")
                    }
                }
                _ = self.navigationController?.popViewController(animated: true)
            }
            else{
                alertWarnning(title: "Thông báo", message: "Chưa có sách mượn.")
                return
            }
        }
        else{
            alertWarnning(title: "Thông báo", message: "Ngày hẹn trả phải lớn hơn ngày mượn.")
        }
    }
    
    func checkNotSame(book: Book) -> Bool{
        let number = bookData.count
        if number > 0{
            for i in 0..<bookData.count{
                if book == bookData[i]{
                    let s = borrowedCount[i] + 1
                    if s <= bookData[i].restnumber{
                        borrowedCount[i] = s
                    }
                    else{
                        alertWarnning(title: "Thông báo", message: "Sách này đã hết không thể mượn thêm.")
                    }
                    return false
                }
            }
        }
        return true
    }
    
    func getBook(ma: String){
        var book = [Book]()
        let fetchRequest:NSFetchRequest<Book> = Book.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "idbook == %@", ma)
        do{
            let searchResults = try Database.getContext().fetch(fetchRequest)
            book = searchResults
            if book.count > 0{
                if book[0].restnumber == 0{
                    alertWarnning(title: "Thông báo", message: "Sách này đã hết không thể mượn thêm.")
                    return
                }
                if checkNotSame(book: book[0]){
                    bookData.append(book[0])
                    borrowedCount.append(1)
                    rentedTotal.append(book[0].rentedprice)
                }
            }
            else{
                alertWarnning(title: "Thông báo", message: "Mã sách này không tồn tại.")
            }
        }
        catch{
            print("Error: \(error)")
        }
    }
    
    func updateBorrowedNumber(ma: String, maThe: String, sl: Int32) -> Bool{
        var dt = [BorrowedDetail]()
        let fetchRequest:NSFetchRequest<BorrowedDetail> = BorrowedDetail.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "book.idbook == %@ && card.code == %@", ma, maThe)
        do{
            let searchResults = try Database.getContext().fetch(fetchRequest)
            dt = searchResults
            if dt.count > 0{
                let book = dt[0].book
                book?.restnumber -= sl
                dt[0].number += sl
                
                let billClassName:String = String(describing: Bill.self)
                let bill:Bill = NSEntityDescription.insertNewObject(forEntityName: billClassName, into: Database.getContext()) as! Bill
                
                bill.billbook = book
                bill.paydate = dpkBorrowed.date as NSDate?
                bill.number = sl
                bill.rentedprice = (book?.rentedprice)!
                
                Database.saveContext()
                return true
            }
        }
        catch{
            print("Error: \(error)")
        }
        return false
    }
    
    func getBorrowedDetails(ma: String){
        let fetchRequest:NSFetchRequest<BorrowedDetail> = BorrowedDetail.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "card.code == %@", ma)
        do{
            let searchResults = try Database.getContext().fetch(fetchRequest)
            detail = searchResults
        }
        catch{
            print("Error: \(error)")
        }
    }
    
    func getCardInfo(ma: String){
        let fetchRequest:NSFetchRequest<LibraryCard> = LibraryCard.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "code == %@", ma)
        do{
            let searchResults = try Database.getContext().fetch(fetchRequest)
            card = searchResults
        }
        catch{
            print("Error: \(error)")
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
