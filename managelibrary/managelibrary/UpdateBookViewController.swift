//
//  UpdateBookViewController.swift
//  managelibrary
//
//  Created by Nguyen Do on 5/17/17.
//
//

import UIKit
import CoreData

class UpdateBookViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var pickerMajors: UIPickerView!
    @IBOutlet weak var txtIdBook: UITextField!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtAuthor: UITextField!
    @IBOutlet weak var txtNumber: UITextField!
    @IBOutlet weak var txtBookshelf: UITextField!
    @IBOutlet weak var txtPrice: UITextField!
    @IBOutlet weak var txtRentedPrice: UITextField!
    @IBOutlet weak var imgBook: UIImageView!
    @IBOutlet weak var txtDescript: UITextView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    
    var majors = [Majors]()
    var majorData: Majors!
    var book: Book!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        addDoneButtonTextField(to: txtIdBook)
        addDoneButtonTextField(to: txtName)
        addDoneButtonTextField(to: txtAuthor)
        addDoneButtonTextField(to: txtNumber)
        addDoneButtonTextField(to: txtBookshelf)
        addDoneButtonTextField(to: txtPrice)
        addDoneButtonTextField(to: txtRentedPrice)
        addDoneButtonTextView(to: txtDescript)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: .UIKeyboardWillHide, object: nil)
        
        // Do any additional setup after loading the view.
        self.pickerMajors.dataSource = self
        self.pickerMajors.delegate = self
        
        imgBook.isUserInteractionEnabled = true
        let tapgetsture = UITapGestureRecognizer(target: self, action: #selector(ImageTapped(sender:)))
        self.imgBook.addGestureRecognizer(tapgetsture)
        
        txtIdBook.text = book.idbook
        txtIdBook.isEnabled = false
        txtName.text = book.name
        txtAuthor.text = book.author
        txtNumber.text = String(book.number)
        txtBookshelf.text = String(book.bookself)
        txtPrice.text = String(book.price)
        txtRentedPrice.text = String(book.rentedprice)
        txtDescript.text = book.descript
        for i in 0 ..< majors.count{
            let major = majors[i]
            if book.majors == major{
                pickerMajors.selectRow(i, inComponent: 0, animated: false)
                majorData = major
            }
        }
        let image : UIImage = UIImage(data: book.image as! Data)!
        imgBook.image = image
    }
    
    func keyboardWillShow(_ notification: Notification){
        scrollView.isScrollEnabled = true
        
        let info = notification.userInfo
        let keyboard = (info?[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue.size
        
        let contentInsets = UIEdgeInsetsMake(0, 0, keyboard.height, 0)
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
    }
    
    func keyboardWillHide(_ notification: Notification){
        let info = notification.userInfo
        let keyboard = (info?[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue.size
        
        let contentInsets = UIEdgeInsetsMake(0, 0, -keyboard.height, 0)
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
        
        self.scrollView.isScrollEnabled = false
    }
    
    func ImageTapped(sender: AnyObject){
        print("Image tapped!")
        let image = UIImagePickerController()
        image.delegate = self
        image.allowsEditing = true
        
        present(image, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let image = info[UIImagePickerControllerOriginalImage] as? UIImage
        imgBook.image = image
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return majors.count
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let major = majors[row]
        return major.name
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let major = majors[row]
        majorData = major
    }
    
    func checkInfo() -> Bool{
        let code = txtIdBook.text!
        if code.isEmpty{
            alertWarnning(title: "Thông báo", message: "Mã sách không được để trống.")
            return false
        }
        let name = txtName.text!
        if name.isEmpty{
            alertWarnning(title: "Thông báo", message: "Tên sách không được để trống.")
            return false
        }
        let author = txtAuthor.text!
        if author.isEmpty{
            alertWarnning(title: "Thông báo", message: "Tên tác giả không được để trống.")
            return false
        }
        let id = txtNumber.text!
        let num = Int(id)
        if num != nil{
            if num! <= 0 {
                alertWarnning(title: "Thông báo", message: "Số lượng sách phải lớn hơn 0.")
                return false
            }
            if (book.restnumber < Int32(num!) && Int32(num!) < book.number){
                alertWarnning(title: "Thông báo", message: "Sách này đã có người mượn. Bạn không thể giảm số lượng.")
                return false
            }
        }else{
            alertWarnning(title: "Thông báo", message: "Số lượng bắt buộc phải nhập vào số nguyên.")
            return false
        }
        let bookself = txtBookshelf.text!
        let numbs = Int(bookself)
        if numbs != nil{
            if numbs! <= 0{
                alertWarnning(title: "Thông báo", message: "Vị trí kệ sách phải lớn hơn 0.")
                return false
            }
        }else{
            alertWarnning(title: "Thông báo", message: "Vị trí kệ sách bắt buộc phải nhập vào số nguyên.")
            return false
        }
        let price = txtPrice.text!
        let prices = Double(price)
        if prices != nil{
            if prices! <= 0{
                alertWarnning(title: "Thông báo", message: "Giá sách phải lớn hơn 0.")
                return false
            }
        }else{
            alertWarnning(title: "Thông báo", message: "Giá sách bắt buộc phải nhập vào số.")
            return false
        }
        let rent = txtRentedPrice.text!
        let rents = Double(rent)
        if rents != nil{
            if rents! <= 0{
                alertWarnning(title: "Thông báo", message: "Giá mượn sách phải lớn hơn 0.")
                return false
            }
        }else{
            alertWarnning(title: "Thông báo", message: "Giá mượn sách bắt buộc phải nhập vào số.")
            return false
        }
        let descript = txtDescript.text!
        if descript.isEmpty{
            alertWarnning(title: "Thông báo", message: "Bạn cần phải mô tả thông tin sách này.")
            return false
        }
        return true
    }
    
    
    @IBAction func editBook(_ sender: Any) {
        let isSuccess = checkInfo()
        if isSuccess == false{
            return
        }
        book.idbook = txtIdBook.text!
        book.name = txtName.text!
        book.author = txtAuthor.text!
        let oldBookNumber = book.number
        let newBookNumber = Int32(txtNumber.text!)!
        book.number = newBookNumber
        var rest = book.restnumber
        rest += (newBookNumber - oldBookNumber)
        book.restnumber = rest
        book.bookself = Int32(txtBookshelf.text!)!
        book.price = Double(txtPrice.text!)!
        book.rentedprice = Double(txtRentedPrice.text!)!
        book.descript = txtDescript.text!
        book.majors = majorData
        let imgData = UIImageJPEGRepresentation(imgBook.image!, 1)
        book.image = imgData as NSData?
        
        Database.saveContext()
        print("Update successfully!")
        _ = self.navigationController?.popViewController(animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
