//
//  UIButton.swift
//  managelibrary
//
//  Created by Nguyen Do on 5/27/17.
//
//

import UIKit
extension UIViewController{
    func addDoneButtonTextField(to control: UITextField){
        // add nude done in toolbar
        let toolBar = UIToolbar()
        toolBar.items = [
            UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil),
            UIBarButtonItem(title: "Xong", style: .done, target: control, action: #selector(UITextField.resignFirstResponder))]
        toolBar.sizeToFit()
        control.inputAccessoryView = toolBar
    }
    func addDoneButtonTextView(to control: UITextView){
        // add nude done in toolbar
        let toolBar = UIToolbar()
        toolBar.items = [
            UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil),
            UIBarButtonItem(title: "Xong", style: .done, target: control, action: #selector(UITextField.resignFirstResponder))]
        toolBar.sizeToFit()
        control.inputAccessoryView = toolBar
    }
    func alertWarnning(title: String, message: String){
        let ok = UIAlertAction(title: "Đồng ý", style: .default, handler: nil)
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(ok)
        self.present(alert, animated: true, completion: nil)
    }
    func containsOnlyLetters(input: String) -> Bool {
        for chr in input.characters 	{
            if (!(chr >= "a" && chr <= "z") && !(chr >= "A" && chr <= "Z") ) {
                return false
            }
        }
        return true
    }
    func comfirmEmail(Email: String) -> Bool {
        let capitalLetterRegEx  = ".*[@]+.*"
        let texttest = NSPredicate(format:"SELF MATCHES %@", capitalLetterRegEx)
        guard texttest.evaluate(with: Email) else { return false }
        return true
    }
    func validateCharacters(password: String) -> Bool {
//        let capitalLetterRegEx  = ".*[A-Z]+.*"
//        let texttest = NSPredicate(format:"SELF MATCHES %@", capitalLetterRegEx)
//        guard texttest.evaluate(with: password) else { return false }
//        
//        let numberRegEx  = ".*[0-9]+.*"
//        let texttest1 = NSPredicate(format:"SELF MATCHES %@", numberRegEx)
//        guard texttest1.evaluate(with: password) else { return false }
//        
//        let specialCharacterRegEx  = ".*[!&^%$#@()/_*+-]+.*"
//        let texttest2 = NSPredicate(format:"SELF MATCHES %@", specialCharacterRegEx)
//        guard texttest2.evaluate(with: password) else { return false }
        let regex = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[$@$!%*?&])[A-Za-z\\d$@$!%*?&]{8,}"
        let isMatched = NSPredicate(format:"SELF MATCHES %@", regex)
        guard isMatched.evaluate(with: password) else { return false}
        
        return true
    }

}
extension UITextField{
    func isEmpty()->Bool{
        return (self.text?.characters.count)! == 0
    }
}
