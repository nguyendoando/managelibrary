//
//  ReturnBookTableViewCell.swift
//  managelibrary
//
//  Created by cao cuong on 6/12/17.
//
//

import UIKit

class ReturnBookTableViewCell: UITableViewCell {

    @IBOutlet weak var lblCardCode: UILabel!
    @IBOutlet weak var lblBookName: UILabel!
    @IBOutlet weak var btnReturnBook: UIButton!
    @IBOutlet weak var lblRentedNumber: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
