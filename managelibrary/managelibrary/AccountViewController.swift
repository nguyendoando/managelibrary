//
//  AccountViewController.swift
//  managelibrary
//
//  Created by Nguyen Do on 5/27/17.
//
//

import UIKit
import CoreData

class AccountViewController: UIViewController, UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var btnMenu: UIBarButtonItem!
    @IBOutlet weak var tableView: UITableView!
    var listAccount = [Account]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if revealViewController() != nil {
            btnMenu.target = revealViewController()
            btnMenu.action = #selector(SWRevealViewController.revealToggle(_:))
        }
        //navigationItem.leftBarButtonItem = nil
        tableView.delegate = self
        tableView.dataSource = self
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getAccount()
        tableView.reloadData()
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listAccount.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "accountCell", for: indexPath) as! AccountTableViewCell
        let lstAccount = listAccount[indexPath.row]
    
        cell.lblEmail.text = lstAccount.email
        cell.lblName.text = lstAccount.name
        let image1: UIImage = UIImage(data: lstAccount.image as! Data)!
        cell.imgAccount.translatesAutoresizingMaskIntoConstraints = false
        cell.imgAccount.layer.cornerRadius = 20
        cell.imgAccount.layer.masksToBounds = true
        cell.imgAccount.layer.borderColor = UIColor.red.cgColor
        cell.imgAccount.image = image1
        return cell
    }
    func getAccount(){
        listAccount.removeAll()
        let fetchRequest: NSFetchRequest<Account> = Account.fetchRequest()
        do{
            let searchResults = try Database.getContext().fetch(fetchRequest)
            listAccount = searchResults
        }
        catch{
            print("Error: \(error)")
        }
    }
    
}
