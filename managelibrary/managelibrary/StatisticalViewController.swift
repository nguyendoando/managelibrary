//
//  StatisticalViewController.swift
//  managelibrary
//
//  Created by Nguyen Do on 5/15/17.
//
//

import UIKit

class StatisticalViewController: UIViewController {

    
    @IBOutlet weak var btnMenu: UIBarButtonItem!
    @IBOutlet weak var dpkDayOfFirsts: UIDatePicker!
    @IBOutlet weak var dpkDayOfEnd: UIDatePicker!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //revealViewController().rearViewRevealWidth = 200
        if revealViewController() != nil {
            btnMenu.target = revealViewController()
            btnMenu.action = #selector(SWRevealViewController.revealToggle(_:))
            
        }

    }
    
    @IBAction func btnStatistical(_ sender: Any) {
        let dayStart = dpkDayOfFirsts.date as NSDate?
        let dayEnd = dpkDayOfEnd.date as NSDate?
        if dayStart?.compare(dayEnd as! Date) == .orderedAscending{
            performSegue(withIdentifier: "Statistical_Segue", sender: self)
        }
        else{
            alertWarnning(title: "Thông báo", message: "Ngày bắt đầu phải nhỏ hơn ngày kết thúc.")
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Statistical_Segue"{
            let vc = segue.destination as! ListStatisticalViewController
            let dayStart = dpkDayOfFirsts.date as NSDate?
            let dayEnd = dpkDayOfEnd.date as NSDate?
            vc.dayStart = dayStart
            vc.dayEnd = dayEnd
        }
    }
}
