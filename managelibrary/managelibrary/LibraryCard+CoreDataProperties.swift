//
//  LibraryCard+CoreDataProperties.swift
//  managelibrary
//
//  Created by cao cuong on 6/4/17.
//
//

import Foundation
import CoreData


extension LibraryCard {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<LibraryCard> {
        return NSFetchRequest<LibraryCard>(entityName: "LibraryCard");
    }

    @NSManaged public var birthday: NSDate?
    @NSManaged public var cardtype: String?
    @NSManaged public var classroom: String?
    @NSManaged public var code: String?
    @NSManaged public var department: String?
    @NSManaged public var expirydate: NSDate?
    @NSManaged public var firstyear: Int32
    @NSManaged public var image: NSData?
    @NSManaged public var lastyear: Int32
    @NSManaged public var name: String?
    @NSManaged public var detail: NSSet?

}

// MARK: Generated accessors for detail
extension LibraryCard {

    @objc(addDetailObject:)
    @NSManaged public func addToDetail(_ value: BorrowedDetail)

    @objc(removeDetailObject:)
    @NSManaged public func removeFromDetail(_ value: BorrowedDetail)

    @objc(addDetail:)
    @NSManaged public func addToDetail(_ values: NSSet)

    @objc(removeDetail:)
    @NSManaged public func removeFromDetail(_ values: NSSet)

}
