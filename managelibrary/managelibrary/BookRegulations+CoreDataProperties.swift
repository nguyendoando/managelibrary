//
//  BookRegulations+CoreDataProperties.swift
//  managelibrary
//
//  Created by cao cuong on 6/4/17.
//
//

import Foundation
import CoreData


extension BookRegulations {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<BookRegulations> {
        return NSFetchRequest<BookRegulations>(entityName: "BookRegulations");
    }

    @NSManaged public var contents: String?

}
