//
//  TeacherTableViewController.swift
//  managelibrary
//
//  Created by cao cuong on 5/30/17.
//
//

import UIKit
import CoreData
class TeacherTableViewController: UITableViewController{

    
    @IBOutlet weak var btnMenu: UIBarButtonItem!
    var teacher = [LibraryCard]()
    var isCheck = false
    var pos = -1
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        if revealViewController() != nil {
            btnMenu.target = revealViewController()
            btnMenu.action = #selector(SWRevealViewController.revealToggle(_:))
        }
        self.tableView.delegate = self
        self.tableView.dataSource = self
        let tap = UITapGestureRecognizer(target: self, action: #selector(doubleTaps))
        tap.numberOfTapsRequired = 2
        tableView.addGestureRecognizer(tap)

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getTeacher()
        self.tableView.reloadData()
    }
    func doubleTaps(){
        isCheck = false
        performSegue(withIdentifier: "detailTeacher_segue", sender: self)
    }
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return teacher.count
    }

    @IBAction func btnAddTeacher(_ sender: Any) {
        isCheck = false
        performSegue(withIdentifier: "addEditTeacher_segue", sender: self)
    }
    @IBAction func btnEditTeacher(_ sender: Any) {
        if(isCheck == true){
            performSegue(withIdentifier: "addEditTeacher_segue", sender: self)
        }
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "teacherCell", for: indexPath) as! TeacherTableViewCell

        let tbteacher = teacher[indexPath.row]
        
        let image: UIImage = UIImage(data: tbteacher.image as! Data)!
        cell.imgTeacher.translatesAutoresizingMaskIntoConstraints = false
        cell.imgTeacher.layer.cornerRadius = 20
        cell.imgTeacher.layer.masksToBounds = true
        cell.imgTeacher.layer.borderColor = UIColor.red.cgColor
        cell.imgTeacher.image = image
        cell.lblMa.text = tbteacher.code
        cell.lblName.text = tbteacher.name
        cell.lblKhoa.text = tbteacher.department

        return cell
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        isCheck = true
        pos = indexPath.row
    }
    
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            Database.getContext().delete(teacher[indexPath.row])
            Database.saveContext()
            teacher.remove(at: indexPath.row)
            tableView.reloadData()
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "addEditTeacher_segue" {
            let cell = segue.destination as! TeacherViewController
            if (isCheck){
                cell.pos = pos
                cell.isCheck = isCheck
                cell.teacher = teacher
            }
        }
        if segue.identifier == "detailTeacher_segue"{
            let vc = segue.destination as! DetailTeacherViewController
            vc.pos = pos
            vc.teacher = teacher
        }

    }
 
    func getTeacher(){
        teacher.removeAll()
        let cardtype = "2"
        let fetchRequest: NSFetchRequest<LibraryCard> = LibraryCard.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "cardtype == %@", cardtype)
        do{
            let searchResults = try Database.getContext().fetch(fetchRequest)
            teacher = searchResults
        }
        catch{
            print("Error: \(error)")
        }
    }

}
