//
//  DetailBookViewController.swift
//  managelibrary
//
//  Created by Nguyen Do on 5/17/17.
//
//

import UIKit

class DetailBookViewController: UIViewController {

    @IBOutlet weak var lblIdBook: UILabel!
    @IBOutlet weak var lblNameBook: UILabel!
    @IBOutlet weak var lblAuthor: UILabel!
    @IBOutlet weak var lblNumber: UILabel!
    @IBOutlet weak var lblRestNumber: UILabel!
    @IBOutlet weak var lblBookshelf: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblRentedPrice: UILabel!
    @IBOutlet weak var imgBook: UIImageView!
    @IBOutlet weak var lblDescript: UILabel!
    
    var book: Book!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblIdBook.text = book.idbook
        lblNameBook.text = book.name
        lblAuthor.text = book.author
        lblNumber.text = String(book.number)
        lblRestNumber.text = String(book.restnumber)
        lblBookshelf.text = String(book.bookself)
        lblPrice.text = String(book.price)
        lblRentedPrice.text = String(book.rentedprice)
        lblDescript.text = book.descript
        let image : UIImage = UIImage(data: book.image as! Data)!
        imgBook.image = image
    }

}
