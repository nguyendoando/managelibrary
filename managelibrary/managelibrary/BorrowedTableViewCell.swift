//
//  BorrowedTableViewCell.swift
//  managelibrary
//
//  Created by cao cuong on 6/4/17.
//
//

import UIKit

class BorrowedTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblCardCode: UILabel!
    @IBOutlet weak var lblBookName: UILabel!
    @IBOutlet weak var lblNumber: UILabel!
    @IBOutlet weak var lblReturnDate: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
