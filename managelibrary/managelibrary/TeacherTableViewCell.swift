//
//  TeacherTableViewCell.swift
//  managelibrary
//
//  Created by cao cuong on 5/30/17.
//
//

import UIKit

class TeacherTableViewCell: UITableViewCell {

    @IBOutlet weak var imgTeacher: UIImageView!
    @IBOutlet weak var lblMa: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblKhoa: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        imgTeacher.layer.cornerRadius = 22
        imgTeacher.layer.borderColor = UIColor.black.cgColor
        imgTeacher.layer.masksToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
