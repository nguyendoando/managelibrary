//
//  StudentTableViewController.swift
//  managelibrary
//
//  Created by cao cuong on 5/30/17.
//
//

import UIKit
import CoreData

class StudentTableViewController: UITableViewController {

    
    @IBOutlet weak var btnMenu: UIBarButtonItem!
    
    var isEdit = false
    var pos = -1
    var students = [LibraryCard]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        if revealViewController() != nil {
            btnMenu.target = revealViewController()
            btnMenu.action = #selector(SWRevealViewController.revealToggle(_:))
        }
        let tap = UITapGestureRecognizer(target: self, action: #selector(doubleTaps))
        tap.numberOfTapsRequired = 2
        tableView.addGestureRecognizer(tap)
    }

    func doubleTaps(){
        isEdit = false
        performSegue(withIdentifier: "studentDetail_segue", sender: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getStudent()
        tableView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return students.count
    }
    
    @IBOutlet weak var btnAddStudent: UIBarButtonItem!
    @IBOutlet weak var btnEditStudent: UIBarButtonItem!
    
    @IBAction func btnAddStudent(_ sender: Any) {
        isEdit = false
        performSegue(withIdentifier: "AddEditStudent_segue", sender: self)
    }
    
    @IBAction func btnEditStudent(_ sender: Any) {
        if isEdit{
            performSegue(withIdentifier: "AddEditStudent_segue", sender: self)
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "AddEditStudent_segue"{
            let vc = segue.destination as! StudentViewController
            if isEdit{
                vc.isEdit = isEdit
                vc.student = students[pos]
            }
        }
        if segue.identifier == "studentDetail_segue"{
            let vc = segue.destination as! DetailStudentViewController
            vc.student = students[pos]
        }
    }
    
    func getStudent(){
        students.removeAll()
        let cardtype = "1"
        let fetchRequest:NSFetchRequest<LibraryCard> = LibraryCard.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "cardtype == %@", cardtype)
        do{
            let searchResults = try Database.getContext().fetch(fetchRequest)
            students = searchResults
        }
        catch{
            print("Error: \(error)")
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "StudentCell", for: indexPath) as! StudentTableViewCell

        // Configure the cell...
        let student = students[indexPath.row]
        cell.lblCode.text = student.code
        cell.lblName.text = student.name
        cell.lblClass.text = student.classroom
        let image : UIImage = UIImage(data: student.image as! Data)!
        cell.imgStudent.image = image
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        isEdit = true
        pos = indexPath.row
    }

    
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    

    
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            Database.getContext().delete(students[indexPath.row])
            Database.saveContext()
            students.remove(at: indexPath.row)
            tableView.reloadData()
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
