//
//  ListBookViewController.swift
//  managelibrary
//
//  Created by Nguyen Do on 5/15/17.
//
//

import UIKit
import CoreData
enum selectedScopeStatistical:Int{
    case name = 0
    case bookArea = 1
}
class ListBookViewController: UIViewController,UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate{

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var btnAddBook: UIButton!
    @IBOutlet weak var listBookTableView: UITableView!
    @IBOutlet weak var btnMenu: UIBarButtonItem!
    
    var majors = [Majors]()
    var isEdit = false
    var pos = -1
    var isCheckDoubleClick = false
    override func viewDidLoad() {
        super.viewDidLoad()
        SearchBarSettup()
        if(GlobaVariable.global == false){
            self.navigationItem.rightBarButtonItems = nil
            //btnAddBook.isHidden = true
        }
        
        revealViewController().rearViewRevealWidth = 200
        btnMenu.target = revealViewController()
        btnMenu.action = #selector(SWRevealViewController.revealToggle(_:))
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(doubleTaps))
        tap.numberOfTapsRequired = 2
        listBookTableView.addGestureRecognizer(tap)
    }
    
    func doubleTaps(){
        isEdit = false
        if( isCheckDoubleClick == true){
            performSegue(withIdentifier: "listBook_Segue", sender: self)
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getMajors()
        listBookTableView.reloadData()
    }
    
    //****** Search bar **********
    
    func SearchBarSettup(){
        let searchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 70))
        searchBar.showsScopeBar = true
        searchBar.scopeButtonTitles = ["Tên ngành","Tên khu vực"]
        searchBar.selectedScopeButtonIndex = 0
        searchBar.delegate = self
        self.tableView.tableHeaderView = searchBar
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        isCheckDoubleClick = false
        if searchText.isEmpty {
            getMajors()
            self.tableView.reloadData()
        }else{
                filterTableView(ind: searchBar.selectedScopeButtonIndex, text: searchText)
        }

    }
    func filterTableView(ind: Int, text:String){
        switch ind {
        case selectedScopeStatistical.name.rawValue:
            majors =  majors.filter({ mod -> Bool in
                //return (mod.name?.lowercased().contains(text.lowercased()))!
                return (mod.name?.lowercased().localizedStandardContains(text.lowercased()))!
            })
            self.tableView.reloadData()
        case selectedScopeStatistical.bookArea.rawValue:
            majors =  majors.filter({ mod -> Bool in
                //return (mod.bookArea?.name?.lowercased().contains(text.lowercased()))!
                return (mod.bookArea?.name?.lowercased().localizedStandardContains(text.lowercased()))!
            })
            self.tableView.reloadData()
        default:
            print("Không có dữ liệu")
        }
    }
    
    //******* End searchBar *********
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return majors.count // count row
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1 // row display
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = listBookTableView.dequeueReusableCell(withIdentifier: "MyListBookCell", for: indexPath) as! ListBookTableViewCell
        
        let majorData = majors[indexPath.row]
        let area = majorData.bookArea! as BookArea
        cell.lblAreaBook.text = area.name
        cell.lblSpecialized.text = majorData.name
        let image : UIImage = UIImage(data: majorData.image as! Data)!
        cell.imgListBook.translatesAutoresizingMaskIntoConstraints = false
        cell.imgListBook.layer.cornerRadius = 21
        cell.imgListBook.layer.masksToBounds = true
        cell.imgListBook.image = image
        cell.btnAddBook.tag = indexPath.row
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            Database.getContext().delete(majors[indexPath.row])
            Database.saveContext()
            majors.remove(at: indexPath.row)
            tableView.reloadData()
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        isCheckDoubleClick = true
        pos = indexPath.row
        isEdit = true
        //performSegue(withIdentifier: "listBook_Segue", sender: self)
    }
    
    @IBAction func btnAddSpecial(_ sender: Any) {
        isEdit = false
        performSegue(withIdentifier: "AddSpecial_segue", sender: self)
    }
    
    @IBAction func btnEditSpecial(_ sender: Any) {
        if isEdit{
            performSegue(withIdentifier: "AddSpecial_segue", sender: self)
        }
    }
    
    @IBAction func addBook(_ sender: AnyObject) {
        isEdit = false
        let row = sender.tag
        pos = row!
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "AddSpecial_segue"{
            let vc = segue.destination as! AddSpecialViewController
            if isEdit{
                vc.pos = pos
                vc.isEdit = isEdit
                vc.majorsData = majors
                isEdit = false
            }
        }
        if segue.identifier == "EditSpecial_segue"{
            let vc = segue.destination as! AddBookViewController
            vc.majors = majors[pos]
        }
        if segue.identifier == "listBook_Segue"{
            let vc = segue.destination as! BookViewController
            vc.majors = majors[pos]
        }
    }
    
    func getMajors(){
        majors.removeAll()
        let fetchRequest:NSFetchRequest<Majors> = Majors.fetchRequest()
        do{
            let searchResults = try Database.getContext().fetch(fetchRequest)
            majors = searchResults
        }
        catch{
            print("Error: \(error)")
        }
    }

}
