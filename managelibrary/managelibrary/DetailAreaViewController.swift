//
//  DetailAreaViewController.swift
//  managelibrary
//
//  Created by Nguyen Do on 5/17/17.
//
//

import UIKit

class DetailAreaViewController: UIViewController {
    @IBOutlet weak var lblNameArea: UILabel!
    @IBOutlet weak var imgImageArea: UIImageView!
    @IBOutlet weak var lblDescription: UILabel!
    
    var pos = -1
    var bookarea = [BookArea]()
    var autoID = 1
    override func viewDidLoad() {
        super.viewDidLoad()
        lblNameArea.text = bookarea[pos].name
        imgImageArea.image = UIImage(data: bookarea[pos].image as! Data)!
        lblDescription.text = bookarea[pos].descript
    }
}
