//
//  RegulationsViewController.swift
//  managelibrary
//
//  Created by Nguyen Do on 5/15/17.
//
//

import UIKit

class RegulationsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var lblCombobox: UITextField!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var btnMenu: UIBarButtonItem!

    let content = ["1. Nội quy thư viện", "2. Quy trình mượn sách", "3. Những quy định về bảo quản tài liệu, tài sản, khen thưởng, kỷ luật"]
    var pos = -1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //revealViewController().rearViewRevealWidth = 200
        if revealViewController() != nil {
            btnMenu.target = revealViewController()
            btnMenu.action = #selector(SWRevealViewController.revealToggle(_:))
            
        }
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.estimatedRowHeight = 56
        self.tableView.rowHeight = UITableViewAutomaticDimension
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1 // display row
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return content.count// number row cell
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "RegulationsCell", for: indexPath) as! RegulationsTableViewCell
        
        cell.lblRules.text = content[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        pos = indexPath.row
        performSegue(withIdentifier: "Rules_segue", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Rules_segue"{
            let vc = segue.destination as! RulesViewController
            let option = pos + 1
            vc.option = option
        }
    }
}
