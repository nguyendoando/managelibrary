//
//  Majors+CoreDataProperties.swift
//  managelibrary
//
//  Created by cao cuong on 6/4/17.
//
//

import Foundation
import CoreData


extension Majors {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Majors> {
        return NSFetchRequest<Majors>(entityName: "Majors");
    }

    @NSManaged public var image: NSData?
    @NSManaged public var name: String?
    @NSManaged public var book: NSSet?
    @NSManaged public var bookArea: BookArea?

}

// MARK: Generated accessors for book
extension Majors {

    @objc(addBookObject:)
    @NSManaged public func addToBook(_ value: Book)

    @objc(removeBookObject:)
    @NSManaged public func removeFromBook(_ value: Book)

    @objc(addBook:)
    @NSManaged public func addToBook(_ values: NSSet)

    @objc(removeBook:)
    @NSManaged public func removeFromBook(_ values: NSSet)

}
