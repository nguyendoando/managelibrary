//
//  Bill+CoreDataProperties.swift
//  managelibrary
//
//  Created by cao cuong on 6/12/17.
//
//

import Foundation
import CoreData


extension Bill {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Bill> {
        return NSFetchRequest<Bill>(entityName: "Bill");
    }

    @NSManaged public var paydate: NSDate?
    @NSManaged public var rentedprice: Double
    @NSManaged public var number: Int32
    @NSManaged public var billbook: Book?

}
