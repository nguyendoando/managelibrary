//
//  menuViewController.swift
//  Mid-termProject
//
//  Created by Nguyen Do on 4/13/17.
//  Copyright © 2017 DoanDo. All rights reserved.
//

import UIKit

class menuViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var tblTableView: UITableView!
    @IBOutlet weak var imgProfile: UIImageView!
    var MenuNameArray:Array = [String]()
    var iconArray:Array = [UIImage]()
    var temp = [String]()
    var iconTemp = [UIImage]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        MenuNameArray = ["Chuyên ngành","Khu vực","Thống kê","Thông báo","Quy định","Tài khoản","Đăng ký","Thẻ sinh viên","Thẻ giáo viên", "Mượn sách", "Trả sách","Đăng xuất","Đăng nhập"]
        iconArray = [UIImage(named:"major-icon64px")!,UIImage(named:"area-icon46px")!,UIImage(named:"statistics-icon64px")!,UIImage(named:"notice-icon64px")!,UIImage(named:"listbook-icon32")!,UIImage(named:"account-icon64")!, UIImage(named:"register-icon64")!,UIImage(named:"student-icon64")!,UIImage(named:"teacher-icon64")!,UIImage(named:"icon-borrow")!,UIImage(named:"icon-lend")!,UIImage(named:"logout-icon64")!,	UIImage(named:"login-icon180")!]
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        temp.removeAll()
        iconTemp.removeAll()
        temp = MenuNameArray
        iconTemp = iconArray
        print("\(iconTemp.count) -- \(temp.count)")
        if(GlobaVariable.global == false){
            temp.remove(at: 11)
            temp.remove(at: 10)
            temp.remove(at: 9)
            temp.remove(at: 8)
            temp.remove(at: 7)
            temp.remove(at: 6)
            temp.remove(at: 5)
            iconTemp.remove(at: 11)
            iconTemp.remove(at: 10)
            iconTemp.remove(at: 9)
            iconTemp.remove(at: 8)
            iconTemp.remove(at: 7)
            iconTemp.remove(at: 6)
            iconTemp.remove(at: 5)
        }
        else{
            if((GlobaVariable.global == true) && (GlobaVariable.glabalAll == false)){

                temp.remove(at: 12)
                temp.remove(at: 6)
                temp.remove(at: 5)
                iconTemp.remove(at: 12)
                iconTemp.remove(at: 6)
                iconTemp.remove(at: 5)
            }else{
                temp.remove(at: 12)
                iconTemp.remove(at: 12)
            }
        }
        self.tblTableView.reloadData()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return temp.count
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuCell", for: indexPath) as! MenuCell
        
        cell.lblMenuname.text! = temp[indexPath.row]
        print(cell.lblMenuname.text!)
        cell.imgIcon.image = iconTemp[indexPath.row]

        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let revealviewcontroller:SWRevealViewController = self.revealViewController()
        
        let cell:MenuCell = tableView.cellForRow(at: indexPath) as! MenuCell
        print(cell.lblMenuname.text!)
        
        if cell.lblMenuname.text! == "Chuyên ngành"
        {
            let mainstoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let newViewcontroller = mainstoryboard.instantiateViewController(withIdentifier: "ListBookViewController") as! ListBookViewController
            let newFrontController = UINavigationController.init(rootViewController: newViewcontroller)
            
            revealviewcontroller.pushFrontViewController(newFrontController, animated: true)
        }
        if cell.lblMenuname.text! == "Khu vực"
        {

            let mainstoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let newViewcontroller = mainstoryboard.instantiateViewController(withIdentifier: "AreaViewController") as! AreaViewController
            let newFrontController = UINavigationController.init(rootViewController: newViewcontroller)
            
            revealviewcontroller.pushFrontViewController(newFrontController, animated: true)
        }
        if cell.lblMenuname.text! == "Thống kê"
        {
            let mainstoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let newViewcontroller = mainstoryboard.instantiateViewController(withIdentifier: "StatisticalViewController") as! StatisticalViewController
            let newFrontController = UINavigationController.init(rootViewController: newViewcontroller)
            
            revealviewcontroller.pushFrontViewController(newFrontController, animated: true)
        }
        if cell.lblMenuname.text! == "Thông báo"
        {
            let mainstoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let newViewcontroller = mainstoryboard.instantiateViewController(withIdentifier: "NoticationsBookViewController") as! NoticationsBookViewController
            let newFrontController = UINavigationController.init(rootViewController: newViewcontroller)
            
            revealviewcontroller.pushFrontViewController(newFrontController, animated: true)
        }
        if cell.lblMenuname.text! == "Quy định"
        {
            let mainstoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let newViewcontroller = mainstoryboard.instantiateViewController(withIdentifier: "RegulationsViewController") as! RegulationsViewController
            let newFrontController = UINavigationController.init(rootViewController: newViewcontroller)
            
            revealviewcontroller.pushFrontViewController(newFrontController, animated: true)
        }
        if cell.lblMenuname.text! == "Tài khoản"
        {
            if(GlobaVariable.glabalAll == true){
                let mainstoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let newViewcontroller = mainstoryboard.instantiateViewController(withIdentifier: "AccountViewController") as! AccountViewController
                let newFrontController = UINavigationController.init(rootViewController: newViewcontroller)
                
                revealviewcontroller.pushFrontViewController(newFrontController, animated: true)
            }else{
                alertWarnning(title: "Thông báo", message: "Bạn không thể sử dụng chức năng này")
            }

        }
        if cell.lblMenuname.text! == "Đăng ký"
        {
                if(GlobaVariable.glabalAll == true){
                    let mainstoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let newViewcontroller = mainstoryboard.instantiateViewController(withIdentifier: "SignupViewController") as! SignupViewController
                    let newFrontController = UINavigationController.init(rootViewController: newViewcontroller)
            
                    revealviewcontroller.pushFrontViewController(newFrontController, animated: true)
                }else{
                    alertWarnning(title: "Thông báo", message: "Bạn không thể sử dụng chức năng này")
                }
        }
        if cell.lblMenuname.text! == "Đăng nhập"
        {
            if(GlobaVariable.global == false){
                print("account taped")
                let mainstoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let newViewcontroller = mainstoryboard.instantiateViewController(withIdentifier: "LoginAccountViewController") as! LoginAccountViewController
                let newFrontController = UINavigationController.init(rootViewController: newViewcontroller)
                revealviewcontroller.pushFrontViewController(newFrontController, animated: true)
            }
        }
        if cell.lblMenuname.text! == "Đăng xuất"
        {
            if(GlobaVariable.global == true || GlobaVariable.glabalAll == true){
                GlobaVariable.glabalAll = false
                GlobaVariable.global = false
                print("Logout taped")
                let mainstoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let newViewcontroller = mainstoryboard.instantiateViewController(withIdentifier: "ListBookViewController") as! ListBookViewController
                let newFrontController = UINavigationController.init(rootViewController: newViewcontroller)
                
                revealviewcontroller.pushFrontViewController(newFrontController, animated: true)
            }else{
                alertWarnning(title: "Thông báo", message: "Bạn không thể sử dụng chức năng này")
            }
        }
        if cell.lblMenuname.text! == "Thẻ sinh viên"
        {

            if(GlobaVariable.global == true){
                print("Thẻ sinh viên taped")
                let mainstoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let newViewcontroller = mainstoryboard.instantiateViewController(withIdentifier: "StudentTableViewController") as! StudentTableViewController
                let newFrontController = UINavigationController.init(rootViewController: newViewcontroller)
                
                revealviewcontroller.pushFrontViewController(newFrontController, animated: true)
            }else{
                alertWarnning(title: "Thông báo", message: "Bạn không thể sử dụng chức năng này")
            }
        }
        if cell.lblMenuname.text! == "Thẻ giáo viên"
        {
            
            if(GlobaVariable.global == true){
                print("Thẻ giáo viên taped")
                let mainstoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let newViewcontroller = mainstoryboard.instantiateViewController(withIdentifier: "TeacherTableViewController") as! TeacherTableViewController
                let newFrontController = UINavigationController.init(rootViewController: newViewcontroller)
                
                revealviewcontroller.pushFrontViewController(newFrontController, animated: true)
            }else{
                alertWarnning(title: "Thông báo", message: "Bạn không thể sử dụng chức năng này")
            }
        }
        if cell.lblMenuname.text! == "Mượn sách"
        {
            
            if(GlobaVariable.global == true){
                print("Mượn sách taped")
                let mainstoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let newViewcontroller = mainstoryboard.instantiateViewController(withIdentifier: "BorrowedTableViewController") as! BorrowedTableViewController
                let newFrontController = UINavigationController.init(rootViewController: newViewcontroller)
                
                revealviewcontroller.pushFrontViewController(newFrontController, animated: true)
            }else{
                alertWarnning(title: "Thông báo", message: "Bạn không thể sử dụng chức năng này")
            }
        }
        if cell.lblMenuname.text! == "Trả sách"
        {
            
            if(GlobaVariable.global == true){
                print("Trả sách taped")
                let mainstoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let newViewcontroller = mainstoryboard.instantiateViewController(withIdentifier: "ReturnBookTableViewController") as! ReturnBookTableViewController
                let newFrontController = UINavigationController.init(rootViewController: newViewcontroller)
                
                revealviewcontroller.pushFrontViewController(newFrontController, animated: true)
            }else{
                alertWarnning(title: "Thông báo", message: "Bạn không thể sử dụng chức năng này")
            }
        }
    }
}
