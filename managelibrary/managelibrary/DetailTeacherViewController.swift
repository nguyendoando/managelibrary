//
//  DetailTeacherViewController.swift
//  managelibrary
//
//  Created by Nguyen Do on 5/29/17.
//
//

import UIKit

class DetailTeacherViewController: UIViewController {

    @IBOutlet weak var imgTeacher: UIImageView!
    @IBOutlet weak var lblMa: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblKhoa: UILabel!
    @IBOutlet weak var lblNgaySinh: UILabel!
    @IBOutlet weak var lblDayEnd: UILabel!
    
    var teacher = [LibraryCard]()
    var pos = -1
    override func viewDidLoad() {
        super.viewDidLoad()

        lblMa.text = teacher[pos].code
        lblName.text = teacher[pos].name
        lblKhoa.text = teacher[pos].department
        let cal = Calendar.current
        let year = cal.component(.year, from: teacher[pos].birthday as! Date)
        let month = cal.component(.month, from: teacher[pos].birthday as! Date)
        let day = cal.component(.day, from: teacher[pos].birthday as! Date)
        lblNgaySinh.text = "\(day) / \(month) / \(year)"
        
        let calExp = Calendar.current
        let yearExp = calExp.component(.year, from: teacher[pos].expirydate as! Date)
        let monthExp = calExp.component(.month, from: teacher[pos].expirydate as! Date)
        let dayExp = calExp.component(.day, from: teacher[pos].expirydate as! Date)
        lblDayEnd.text = "\(dayExp) / \(monthExp) / \(yearExp)"
        imgTeacher.image = UIImage(data: teacher[pos].image as! Data)!
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
