//
//  Account+CoreDataProperties.swift
//  managelibrary
//
//  Created by cao cuong on 6/4/17.
//
//

import Foundation
import CoreData


extension Account {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Account> {
        return NSFetchRequest<Account>(entityName: "Account");
    }

    @NSManaged public var email: String?
    @NSManaged public var image: NSData?
    @NSManaged public var name: String?
    @NSManaged public var password: String?

}
