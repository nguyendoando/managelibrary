//
//  DetailStudentViewController.swift
//  managelibrary
//
//  Created by Nguyen Do on 5/29/17.
//
//

import UIKit

class DetailStudentViewController: UIViewController {
    
    @IBOutlet weak var imgStudent: UIImageView!
    @IBOutlet weak var lblCode: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblClass: UILabel!
    @IBOutlet weak var lblDepartment: UILabel!
    @IBOutlet weak var lblBirthday: UILabel!
    @IBOutlet weak var lblCourses: UILabel!
    @IBOutlet weak var lblExp: UILabel!
    
    var student: LibraryCard!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        lblCode.text = student.code
        lblName.text = student.name
        let cal = Calendar.current
        let year = cal.component(.year, from: student.birthday as! Date)
        let month = cal.component(.month, from: student.birthday as! Date)
        let day = cal.component(.day, from: student.birthday as! Date)
        lblBirthday.text = "\(day) / \(month) / \(year)"
        lblDepartment.text = student.department
        lblClass.text = student.classroom
        lblCourses.text = String(student.firstyear) + " - " + String(student.lastyear)
        
        let calExp = Calendar.current
        let yearExp = calExp.component(.year, from: student.expirydate as! Date)
        let monthExp = calExp.component(.month, from: student.expirydate as! Date)
        let dayExp = calExp.component(.day, from: student.expirydate as! Date)
        lblExp.text = "\(dayExp) / \(monthExp) / \(yearExp)"
        
        let image : UIImage = UIImage(data: student.image as! Data)!
        imgStudent.image = image
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
