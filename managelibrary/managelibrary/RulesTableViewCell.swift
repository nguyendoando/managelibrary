//
//  RulesTableViewCell.swift
//  managelibrary
//
//  Created by cao cuong on 5/29/17.
//
//

import UIKit

class RulesTableViewCell: UITableViewCell {
    
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var lblRules: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        if(GlobaVariable.glabalAll == false){
            btnEdit.isHidden = true
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
