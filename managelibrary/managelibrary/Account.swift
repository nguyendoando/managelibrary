//
//  Account.swift
//  managelibrary
//
//  Created by cao cuong on 5/18/17.
//
//

class Account{
    var gmail = ""
    var tendangnhap = ""
    var matkhau = ""
    var masach = ""
    var mathongbaoquahan = ""
    var mathongke = ""
    var makhuvuc = ""
    
    init(){
        
    }
    
    init(gmail: String, tendangnhap: String, matkhau: String, masach: String, mathongbaoquahan: String, mathongke: String, makhuvuc: String){
        self.gmail = gmail
        self.tendangnhap = tendangnhap
        self.matkhau = matkhau
        self.masach = masach
        self.mathongbaoquahan = mathongbaoquahan
        self.mathongke = mathongke
        self.makhuvuc = makhuvuc
    }
}
