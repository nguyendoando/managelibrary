//
//  NoticationsBookViewController.swift
//  managelibrary
//
//  Created by Nguyen Do on 5/17/17.
//
//

import UIKit
import CoreData

class NoticationsBookViewController: UIViewController, UITableViewDataSource ,UITableViewDelegate {

    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var btnMenu: UIBarButtonItem!
    
    var detail = [BorrowedDetail]()
    var newDate: NSDate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.delegate = self
        self.tableView.dataSource = self
        //revealViewController().rearViewRevealWidth = 200
        if revealViewController() != nil {
            btnMenu.target = revealViewController()
            btnMenu.action = #selector(SWRevealViewController.revealToggle(_:))
            
        }
        getBorrowedDetails()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return detail.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NoticationsCell", for: indexPath) as! NoticationsBookTableViewCell
        
        let borrowedDetail = detail[indexPath.row]
        let card = borrowedDetail.card! as LibraryCard
        cell.lblMSSV.text = card.code! + " - " + card.name!
        let book = borrowedDetail.book! as Book
        cell.lblBookCode.text = book.idbook! + " - " + book.name!
        
        let cal = Calendar.current
        let year = cal.component(.year, from: borrowedDetail.borroweddate as! Date)
        let month = cal.component(.month, from: borrowedDetail.borroweddate as! Date)
        let day = cal.component(.day, from: borrowedDetail.borroweddate as! Date)
        cell.lblDayBorrow.text = "Ngày mượn: \(day) / \(month) / \(year)"
        
        let calSend = Calendar.current
        let yearSend = calSend.component(.year, from: borrowedDetail.returneddate as! Date)
        let monthSend = calSend.component(.month, from: borrowedDetail.returneddate as! Date)
        let daySend = calSend.component(.day, from: borrowedDetail.returneddate as! Date)
        cell.lblSend.text = "Ngày hẹn trả: \(daySend) / \(monthSend) / \(yearSend)"
        
        cell.lblNumber.text = "Số lượng mượn: " + String(borrowedDetail.number)
        let calDayNumber = Calendar.current
        let dateStart = borrowedDetail.returneddate as! Date
        let dateEnd = newDate as! Date
        let components = calDayNumber.dateComponents([.day], from: dateStart, to: dateEnd)
        cell.lblNumberDayEnd.text = "Số ngày quá hạn: " + String(describing: components.day!)
        
        return cell
    }
    
    func getBorrowedDetails(){
        detail.removeAll()
        var cal = Calendar.current
        cal.timeZone = NSTimeZone(forSecondsFromGMT: 7) as TimeZone
        newDate = cal.date(bySettingHour: 0, minute: 0, second: 0, of: Date()) as NSDate?
        let fetchRequest:NSFetchRequest<BorrowedDetail> = BorrowedDetail.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "returneddate <= %@", newDate!)
        do{
            let searchResults = try Database.getContext().fetch(fetchRequest)
            detail = searchResults
        }
        catch{
            print("Error: \(error)")
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
