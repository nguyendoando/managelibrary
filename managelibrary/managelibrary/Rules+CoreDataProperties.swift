//
//  Rules+CoreDataProperties.swift
//  managelibrary
//
//  Created by cao cuong on 6/4/17.
//
//

import Foundation
import CoreData


extension Rules {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Rules> {
        return NSFetchRequest<Rules>(entityName: "Rules");
    }

    @NSManaged public var contents: String?

}
