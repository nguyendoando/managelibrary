//
//  AddSpecialViewController.swift
//  managelibrary
//
//  Created by Nguyen Do on 5/17/17.
//
//

import UIKit
import CoreData

class AddSpecialViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate  {

    
    @IBOutlet weak var btnEditSpecial: UIButton!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var pickerArea: UIPickerView!
    @IBOutlet weak var imgMajors: UIImageView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    var bookarea = [BookArea]()
    var areaData: BookArea!
    var isEdit = false
    var pos = -1
    var majorsData = [Majors]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addDoneButtonTextField(to: txtName)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: .UIKeyboardWillHide, object: nil)
        // Do any additional setup after loading the view.
        getBookArea()
        
        self.pickerArea.dataSource = self
        self.pickerArea.delegate = self
        
        if bookarea.count == 0{
            pickerArea.isHidden = true
        }else{
            areaData = bookarea[0]
        }
        imgMajors.isUserInteractionEnabled = true
        let tapgetsture = UITapGestureRecognizer(target: self, action: #selector(ImageTapped(sender:)))
        self.imgMajors.addGestureRecognizer(tapgetsture)
        
        if isEdit{
            btnEditSpecial.setTitle("Sửa", for: .normal)
            txtName.text = majorsData[pos].name
            for i in 0 ..< bookarea.count{
                let area = bookarea[i]
                if majorsData[pos].bookArea == area{
                    pickerArea.selectRow(i, inComponent: 0, animated: false)
                    areaData = area
                }
            }
            let image : UIImage = UIImage(data: majorsData[pos].image as! Data)!
            imgMajors.image = image
        }
    }

    func keyboardWillShow(_ notification: Notification){
        scrollView.isScrollEnabled = true
        
        let info = notification.userInfo
        let keyboard = (info?[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue.size
        
        let contentInsets = UIEdgeInsetsMake(0, 0, keyboard.height, 0)
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
    }
    
    func keyboardWillHide(_ notification: Notification){
        let info = notification.userInfo
        let keyboard = (info?[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue.size
        
        let contentInsets = UIEdgeInsetsMake(0, 0, -keyboard.height, 0)
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
        
        self.scrollView.isScrollEnabled = false
    }
    
    func ImageTapped(sender: AnyObject){
        print("Image tapped!")
        let image = UIImagePickerController()
        image.delegate = self
        image.allowsEditing = true
        
        present(image, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let image = info[UIImagePickerControllerOriginalImage] as? UIImage
        imgMajors.image = image
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func checkInfo()->Bool{
        let name = txtName.text!
        if name.isEmpty{
            alertWarnning(title: "Thông báo" , message: "Mời nhập tên chuyên ngành")
            return false
        }
        if bookarea.count == 0{
            alertWarnning(title: "Thông báo" , message: "Mời chọn khu vực")
            return false
        }
        return true
    }
    
    @IBAction func addMajors(_ sender: Any) {
        let isSuccess = checkInfo()
        if isSuccess == false{
            return
        }
        let majorsClassName:String = String(describing: Majors.self)
        if isEdit{
            majorsData[pos].name = txtName.text!
            majorsData[pos].bookArea = areaData
            let imgData = UIImageJPEGRepresentation(imgMajors.image!, 1)
            majorsData[pos].image = imgData as NSData?
            
            Database.saveContext()
            print("Update successfully!")
        }
        else{
            let majors:Majors = NSEntityDescription.insertNewObject(forEntityName: majorsClassName, into: Database.getContext()) as! Majors
            
            majors.name = txtName.text!
            majors.bookArea = areaData
            let imgData = UIImageJPEGRepresentation(imgMajors.image!, 1)
            majors.setValue(imgData, forKey: "image")
            
            Database.saveContext()
            print("Insert successfully!")
        }
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return bookarea.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let area = bookarea[row]
        return area.name
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let area = bookarea[row]
        areaData = area
    }
    
    func getBookArea(){
        let fetchRequest:NSFetchRequest<BookArea> = BookArea.fetchRequest()
        do{
            let searchResults = try Database.getContext().fetch(fetchRequest)
            bookarea = searchResults
        }
        catch{
            print("Error: \(error)")
        }
    }
    

}
