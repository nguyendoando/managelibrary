//
//  BookTableViewCell.swift
//  managelibrary
//
//  Created by Nguyen Do on 5/17/17.
//
//

import UIKit

class BookTableViewCell: UITableViewCell {

    @IBOutlet weak var imgImageBook: UIImageView!
    @IBOutlet weak var lblNameBook: UILabel!
    @IBOutlet weak var lblAuthor: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblNumberCount: UILabel!
    @IBOutlet weak var lblBookShelf: UILabel!
    @IBOutlet weak var btnEditBook: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        if(GlobaVariable.global == false){
            btnEditBook.isHidden = true
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
