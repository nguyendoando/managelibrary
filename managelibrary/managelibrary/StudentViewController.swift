//
//  StudentViewController.swift
//  managelibrary
//
//  Created by cao cuong on 5/30/17.
//
//

import UIKit
import CoreData

class StudentViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var btnAddStudent: UIButton!
    @IBOutlet weak var imgStudent: UIImageView!
    @IBOutlet weak var txtCode: UITextField!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var dateBirthday: UIDatePicker!
    @IBOutlet weak var txtDepartment: UITextField!
    @IBOutlet weak var txtClass: UITextField!
    @IBOutlet weak var txtFirstYear: UITextField!
    @IBOutlet weak var txtLastYear: UITextField!
    @IBOutlet weak var dateExp: UIDatePicker!
    @IBOutlet weak var scrollView: UIScrollView!
    
    var isEdit = false
    var student: LibraryCard!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        addDoneButtonTextField(to: txtCode)
        addDoneButtonTextField(to: txtName)
        addDoneButtonTextField(to: txtDepartment)
        addDoneButtonTextField(to: txtClass)
        addDoneButtonTextField(to: txtFirstYear)
        addDoneButtonTextField(to: txtLastYear)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: .UIKeyboardWillHide, object: nil)
        
        imgStudent.isUserInteractionEnabled = true
        let tapgetsture = UITapGestureRecognizer(target: self, action: #selector(ImageTapped(sender:)))
        self.imgStudent.addGestureRecognizer(tapgetsture)
        
        if isEdit{
            btnAddStudent.setTitle("Sửa", for: .normal)
            txtCode.text = student.code
            txtCode.isEnabled = false
            txtName.text = student.name
            dateBirthday.date = student.birthday as! Date
            txtDepartment.text = student.department
            txtClass.text = student.classroom
            txtFirstYear.text = String(student.firstyear)
            txtLastYear.text = String(student.lastyear)
            dateExp.date = student.expirydate as! Date
            
            let image : UIImage = UIImage(data: student.image as! Data)!
            imgStudent.image = image
        }
    }

    func keyboardWillShow(_ notification: Notification){
        scrollView.isScrollEnabled = true
        
        let info = notification.userInfo
        let keyboard = (info?[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue.size
        
        let contentInsets = UIEdgeInsetsMake(0, 0, keyboard.height, 0)
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
    }
    
    func keyboardWillHide(_ notification: Notification){
        let info = notification.userInfo
        let keyboard = (info?[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue.size
        
        let contentInsets = UIEdgeInsetsMake(0, 0, -keyboard.height, 0)
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
        
        self.scrollView.isScrollEnabled = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func ImageTapped(sender: AnyObject){
        print("Image tapped!")
        let image = UIImagePickerController()
        image.delegate = self
        image.allowsEditing = true
        
        present(image, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let image = info[UIImagePickerControllerOriginalImage] as? UIImage
        imgStudent.image = image
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func checkInfo() -> Bool{
        let code = txtCode.text!
        if code.isEmpty{
            alertWarnning(title: "Thông báo", message: "Mã sinh viên không được để trống.")
            return false
        }
        if isEdit == false{
            var students = [LibraryCard]()
            let fetchRequest:NSFetchRequest<LibraryCard> = LibraryCard.fetchRequest()
            fetchRequest.predicate = NSPredicate(format: "code == %@", code)
            do{
                let searchResults = try Database.getContext().fetch(fetchRequest)
                students = searchResults
                if students.count > 0{
                    alertWarnning(title: "Thông báo", message: "Mã thẻ này đã tồn tại.")
                    return false
                }
            }
            catch{
                print("Error: \(error)")
            }
        }
        let name = txtName.text!
        if name.isEmpty{
            alertWarnning(title: "Thông báo", message: "Tên sinh viên không được để trống.")
            return false
        }
        let depart = txtDepartment.text!
        if depart.isEmpty{
            alertWarnning(title: "Thông báo", message: "Khoa không được để trống.")
            return false
        }
        let classroom = txtClass.text!
        if classroom.isEmpty{
            alertWarnning(title: "Thông báo", message: "Lớp học không được để trống.")
            return false
        }
        let firstyear = txtFirstYear.text!
        if firstyear.isEmpty{
            alertWarnning(title: "Thông báo", message: "Năm đầu không được để trống.")
            return false
        }
        let first = Int(firstyear)
        if first != nil{
            
        }else{
            alertWarnning(title: "Thông báo", message: "Năm đầu bắt buộc phải nhập vào số nguyên.")
            return false
        }
        
        let lastyear = txtLastYear.text!
        if lastyear.isEmpty{
            alertWarnning(title: "Thông báo", message: "Năm cuối không được để trống.")
            return false
        }
        let last = Int(lastyear)
        if last != nil{
            
        }else{
            alertWarnning(title: "Thông báo", message: "Năm cuối bắt buộc phải nhập vào số nguyên.")
            return false
        }
        
        return true
    }
    
    @IBAction func addStudent(_ sender: Any) {
        let isSuccess = checkInfo()
        if isSuccess == false{
            return
        }
        let studentClassName:String = String(describing: LibraryCard.self)
        
        if isEdit{
            student.code = txtCode.text!
            student.name = txtName.text!
            student.birthday = dateBirthday.date as NSDate?
            student.department = txtDepartment.text!
            student.classroom = txtClass.text!
            student.firstyear = Int32(txtFirstYear.text!)!
            student.lastyear = Int32(txtLastYear.text!)!
            student.expirydate = dateExp.date as NSDate?
            
            let imgData = UIImageJPEGRepresentation(imgStudent.image!, 1)
            student.image = imgData as NSData?
            
            Database.saveContext()
            print("Update successfully!")
        }
        else{
            let st:LibraryCard = NSEntityDescription.insertNewObject(forEntityName: studentClassName, into: Database.getContext()) as! LibraryCard
            
            st.code = txtCode.text!
            st.name = txtName.text!
            st.birthday = dateBirthday.date as NSDate?
            st.department = txtDepartment.text!
            st.classroom = txtClass.text!
            st.firstyear = Int32(txtFirstYear.text!)!
            st.lastyear = Int32(txtLastYear.text!)!
            st.expirydate = dateExp.date as NSDate?
            st.cardtype = "1"
            
            let imgData = UIImageJPEGRepresentation(imgStudent.image!, 1)
            st.setValue(imgData, forKey: "image")
            
            Database.saveContext()
            print("Insert successfully!")
        }
        
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
