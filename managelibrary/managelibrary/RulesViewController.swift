//
//  RulesViewController.swift
//  managelibrary
//
//  Created by cao cuong on 5/29/17.
//
//

import UIKit
import CoreData

class RulesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    
    var rules = [Rules]()
    var bookReg = [BookRegulations]()
    var bookRules = [BookRules]()
    var isEdit = false
    var pos = -1
    var option = -1
    var numberRow = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.estimatedRowHeight = 37
        self.tableView.rowHeight = UITableViewAutomaticDimension
        if(GlobaVariable.glabalAll == true){
            self.navigationItem.setRightBarButton(UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addRules(sender:))), animated: true)
        }
    }

    func addRules(sender: UIBarButtonItem) {
        isEdit = false
        performSegue(withIdentifier: "AddRules_Segue", sender: self)
    }
    @IBAction func editRules(_ sender: AnyObject) {
        pos = sender.tag
        isEdit = true
        performSegue(withIdentifier: "AddRules_Segue", sender: self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if option == 1{
            getRules()
        }
        if option == 2{
            getBookReg()
        }
        if option == 3{
            getBookRules()
        }
        self.tableView.reloadData()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1 // display row
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if option == 1{
            numberRow = rules.count
        }
        if option == 2{
            numberRow = bookReg.count
        }
        if option == 3{
            numberRow = bookRules.count
        }
        return numberRow// number row cell
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "RulesCell", for: indexPath) as! RulesTableViewCell
        let stt = indexPath.row + 1
        if option == 1{
            cell.lblRules.text = "\(stt). " + rules[indexPath.row].contents!
        }
        if option == 2{
            cell.lblRules.text = "\(stt). " + bookReg[indexPath.row].contents!
        }
        if option == 3{
            cell.lblRules.text = "\(stt). " + bookRules[indexPath.row].contents!
        }
        cell.btnEdit.tag = indexPath.row
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            if option == 1{
                Database.getContext().delete(rules[indexPath.row])
                Database.saveContext()
                rules.remove(at: indexPath.row)
                self.tableView.reloadData()
            }
            if option == 2{
                Database.getContext().delete(bookReg[indexPath.row])
                Database.saveContext()
                bookReg.remove(at: indexPath.row)
                self.tableView.reloadData()
            }
            if option == 3{
                Database.getContext().delete(bookRules[indexPath.row])
                Database.saveContext()
                bookRules.remove(at: indexPath.row)
                self.tableView.reloadData()
            }
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "AddRules_Segue"{
            let vc = segue.destination as! AddRulesViewController
            vc.option = option
            if isEdit{
                if option == 1{
                    vc.rule = rules[pos]
                }
                if option == 2{
                    vc.bookReg = bookReg[pos]
                }
                if option == 3{
                    vc.bookRule = bookRules[pos]
                }
                vc.isEdit = true
            }
        }
    }
    
    func getRules(){
        rules.removeAll()
        let fetchRequest:NSFetchRequest<Rules> = Rules.fetchRequest()
        do{
            let searchResults = try Database.getContext().fetch(fetchRequest)
            rules = searchResults
        }
        catch{
            print("Error: \(error)")
        }
    }
    
    func getBookReg(){
        bookReg.removeAll()
        let fetchRequest:NSFetchRequest<BookRegulations> = BookRegulations.fetchRequest()
        do{
            let searchResults = try Database.getContext().fetch(fetchRequest)
            bookReg = searchResults
        }
        catch{
            print("Error: \(error)")
        }
    }
    
    func getBookRules(){
        bookRules.removeAll()
        let fetchRequest:NSFetchRequest<BookRules> = BookRules.fetchRequest()
        do{
            let searchResults = try Database.getContext().fetch(fetchRequest)
            bookRules = searchResults
        }
        catch{
            print("Error: \(error)")
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
