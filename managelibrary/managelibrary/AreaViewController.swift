//
//  AreaViewController.swift
//  managelibrary
//
//  Created by cao cuong on 5/25/17.
//
//

import UIKit
import CoreData
enum selectedScope:Int{
    case name = 0
    case descript = 1
}
class AreaViewController: UIViewController,UITableViewDataSource ,UITableViewDelegate,UISearchBarDelegate{

    @IBOutlet weak var btnMenu: UIBarButtonItem!
    @IBOutlet weak var tableView: UITableView!
    
    var bookarea = [BookArea]()
    var isEdit = false
    var pos = -1
    var isCheckDoubleClick = false
    var searchActive : Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        SearchBarSettup()
        if(GlobaVariable.global == false)
        {
            navigationItem.rightBarButtonItems = nil
        }
        if revealViewController() != nil {
            btnMenu.target = revealViewController()
            btnMenu.action = #selector(SWRevealViewController.revealToggle(_:))
            
        }
        self.title = "KHU VỰC"
        self.tableView.delegate = self
        self.tableView.dataSource = self
        let tap = UITapGestureRecognizer(target: self, action: #selector(doubleTaps))
        tap.numberOfTapsRequired = 2
        tableView.addGestureRecognizer(tap)
    }
    func doubleTaps(){
        isEdit = false
        if( isCheckDoubleClick == true){
            performSegue(withIdentifier: "DetailArea_segue", sender: self)
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getBookArea()
        tableView.reloadData()
    }
    
    //****** Search bar **********
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchActive = true
    }
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchActive = false
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = true
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = true
    }

    func SearchBarSettup(){
        let searchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 70))
        searchBar.showsScopeBar = true
        searchBar.scopeButtonTitles = ["Tên","Mô tả"]
        searchBar.selectedScopeButtonIndex = 0
        searchBar.delegate = self
        self.tableView.tableHeaderView = searchBar
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        isCheckDoubleClick = false
        if searchText.isEmpty {
            getBookArea()
            self.tableView.reloadData()
        }else{
            filterTableView(ind: searchBar.selectedScopeButtonIndex, text: searchText)
        }
    }
    func filterTableView(ind: Int, text:String){
        switch ind {
        case selectedScope.name.rawValue:
            bookarea =  bookarea.filter({ mod -> Bool in
                //return (mod.name?.lowercased().contains(text.lowercased()))!
                return (mod.name?.lowercased().localizedStandardContains(text.lowercased()))!
            })
            self.tableView.reloadData()
        case selectedScope.descript.rawValue:
            bookarea =  bookarea.filter({ mod -> Bool in
                //return (mod.descript?.lowercased().contains(text.lowercased()))!
                return (mod.descript?.lowercased().localizedStandardContains(text.lowercased()))!
            })
            self.tableView.reloadData()
        default:
            print("Không có dữ liệu")
        }
    }
    
    //******* End searchBar *********
    
    @IBAction func addArea(_ sender: Any) {
        isEdit = false
        performSegue(withIdentifier: "AddEdit_segue", sender: self)
    }
    @IBAction func editArea(_ sender: Any) {
        if isEdit{
            performSegue(withIdentifier: "AddEdit_segue", sender: self)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "AddEdit_segue"{
            let vc = segue.destination as! AddEditAreaViewController
            if isEdit{
                vc.pos = pos
                vc.isEdit = isEdit
                vc.bookarea = bookarea
                isEdit = false
            }
        }
        if segue.identifier == "DetailArea_segue"{
            let vc = segue.destination as! DetailAreaViewController
            vc.pos = pos
            vc.bookarea = bookarea
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return bookarea.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AreaCell", for: indexPath) as! AreaTableViewCell
            let area = bookarea[indexPath.row]
            cell.lblNameArea.text = area.name
            cell.lblDescription.text = area.descript
            let image : UIImage = UIImage(data: area.image as! Data)!
            cell.imgAreaBook.translatesAutoresizingMaskIntoConstraints = false
            cell.imgAreaBook.layer.cornerRadius = 20
            cell.imgAreaBook.layer.masksToBounds = true
            cell.imgAreaBook.image = image
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        pos = indexPath.row
        isCheckDoubleClick = true
        isEdit = true
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            Database.getContext().delete(bookarea[indexPath.row])
            Database.saveContext()
            bookarea.remove(at: indexPath.row)
            tableView.reloadData()
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }
    }
    
    func getBookArea(){
        bookarea.removeAll()
        let fetchRequest:NSFetchRequest<BookArea> = BookArea.fetchRequest()
        do{
            let searchResults = try Database.getContext().fetch(fetchRequest)
            bookarea = searchResults
        }
        catch{
            print("Error: \(error)")
        }
    }
}


