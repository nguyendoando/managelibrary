//
//  ListStatisticalViewController.swift
//  managelibrary
//
//  Created by Nguyen Do on 5/17/17.
//
//

import UIKit
import CoreData

class ListStatisticalViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    var bills = [Bill]()
    var dayStart: NSDate?
    var dayEnd: NSDate?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.tableView.delegate = self
        self.tableView.dataSource = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getBills()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return bills.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "CellStatictical", for: indexPath) as! StatisticalTableViewCell
        
        let bill = bills[indexPath.row]
        let book = bill.billbook! as Book
        cell.lblBookName.text = book.name
        
        let cal = Calendar.current
        let year = cal.component(.year, from: bill.paydate as! Date)
        let month = cal.component(.month, from: bill.paydate as! Date)
        let day = cal.component(.day, from: bill.paydate as! Date)
        cell.lblBorrowedDate.text = "\(day) / \(month) / \(year)"
        
        cell.lblNumber.text = String(bill.number)
        let total = Double(bill.number) * bill.rentedprice
        cell.lblTotal.text = String(total)
        let image : UIImage = UIImage(data: book.image as! Data)!
        cell.imgBook.image = image
        
        return cell
    }
    
    func getBills(){
        bills.removeAll()
        let fetchRequest:NSFetchRequest<Bill> = Bill.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "paydate >= %@ && paydate <= %@", dayStart!, dayEnd!)
        do{
            let searchResults = try Database.getContext().fetch(fetchRequest)
            bills = searchResults
        }
        catch{
            print("Error: \(error)")
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
