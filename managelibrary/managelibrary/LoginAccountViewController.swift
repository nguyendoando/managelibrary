//
//  AccountViewController.swift
//  managelibrary
//
//  Created by Nguyen Do on 5/15/17.
//
//

import UIKit
import CoreData

class LoginAccountViewController: UIViewController {
    
    
    @IBOutlet weak var btnAddAccount: UIBarButtonItem!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var btnMenu: UIBarButtonItem!
    @IBOutlet weak var txtNameAccount: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var imgAccount: UIImageView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    var listAcount = [Account]()
    override func viewDidLoad() {
        super.viewDidLoad()

        btnLogin.layer.masksToBounds = true
        btnLogin.layer.cornerRadius = 8
    
        addDoneButtonTextField(to: txtNameAccount)
        addDoneButtonTextField(to: txtPassword)
        txtPassword.isSecureTextEntry = true
        //revealViewController().rearViewRevealWidth = 200
        if revealViewController() != nil {
            btnMenu.target = revealViewController()
            btnMenu.action = #selector(SWRevealViewController.revealToggle(_:))
        }
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: .UIKeyboardWillHide, object: nil)
    }
    
    func keyboardWillShow(_ notification: Notification){
        scrollView.isScrollEnabled = true
        
        let info = notification.userInfo
        let keyboard = (info?[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue.size
        
        let contentInsets = UIEdgeInsetsMake(0, 0, keyboard.height, 0)
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
    }
    
    func keyboardWillHide(_ notification: Notification){
        let info = notification.userInfo
        let keyboard = (info?[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue.size
        
        let contentInsets = UIEdgeInsetsMake(0, 0, -keyboard.height, 0)
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
        
        self.scrollView.isScrollEnabled = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getAccount()
    }
    
    func check() -> Bool{
        let name = txtNameAccount.text!
        if name.isEmpty {
            alertWarnning(title: "Thông báo", message: "Tên tài khoản không được trống")
            return false
        }
        let password = txtPassword.text!
        if password.isEmpty {
            alertWarnning(title: "Thông báo", message: "Tên tài khoản không được trống")
            return false
        }
        return true
    }
    @IBAction func btnLoginAccount(_ sender: Any) {
        let isCheck = check()
        if isCheck == false{
            return
        }
        let name = txtNameAccount.text!
        let password = txtPassword.text!
        let ix = listAcount.count
        if((name == "admin") && (password == "admin")){
            GlobaVariable.global = true
            GlobaVariable.glabalAll = true
        }else{
            for i in 0 ..< ix {
                if(((name == listAcount[i].name) || (name == listAcount[i].email)) && (password == listAcount[i].password)){
                    GlobaVariable.global = true
                    GlobaVariable.glabalAll = false
                    break
                }else{
                    if(i == (ix-1)){
                        alertWarnning(title: "Thông báo", message: "Tên tài khoản hoặc mật khẩu không chính xác")
                        return
                    }
                }
            }
        }
    }
    func getAccount(){
        listAcount.removeAll()
        let fetchRequest: NSFetchRequest<Account> = Account.fetchRequest()
        do{
            let searchResults = try Database.getContext().fetch(fetchRequest)
            listAcount = searchResults
        }
        catch{
            print("Error: \(error)")
        }
    }

}
