//
//  TeacherViewController.swift
//  managelibrary
//
//  Created by cao cuong on 5/30/17.
//
//

import UIKit
import CoreData
class TeacherViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    
    @IBOutlet weak var dpkEndDay: UIDatePicker!
    @IBOutlet weak var dpkBirthDay: UIDatePicker!
    @IBOutlet weak var imgTeacher: UIImageView!
    @IBOutlet weak var txtMa: UITextField!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtKhoa: UITextField!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    
    var teacher = [LibraryCard]()
    var pos = -1
    var isCheck = false
    override func viewDidLoad() {
        super.viewDidLoad()

        addDoneButtonTextField(to: txtMa)
        addDoneButtonTextField(to: txtName)
        addDoneButtonTextField(to: txtKhoa)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: .UIKeyboardWillHide, object: nil)
        
        imgTeacher.isUserInteractionEnabled = true
        if(isCheck == true){
            btnEdit.setTitle("Sửa", for: .normal)
            txtMa.text = teacher[pos].code
            txtMa.isEnabled = false
            txtName.text = teacher[pos].name
            txtKhoa.text = teacher[pos].department
            dpkBirthDay.date = teacher[pos].birthday as! Date
            dpkEndDay.date = teacher[pos].expirydate as! Date
            
            let image : UIImage = UIImage(data: teacher[pos].image as! Data)!
            imgTeacher.image = image

        }
        let tapgetsture = UITapGestureRecognizer(target: self, action: #selector(ImageTapped(sender:)))
        self.imgTeacher.addGestureRecognizer(tapgetsture)

    }
    
    func keyboardWillShow(_ notification: Notification){
        scrollView.isScrollEnabled = true
        
        let info = notification.userInfo
        let keyboard = (info?[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue.size
        
        let contentInsets = UIEdgeInsetsMake(0, 0, keyboard.height, 0)
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
    }
    
    func keyboardWillHide(_ notification: Notification){
        let info = notification.userInfo
        let keyboard = (info?[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue.size
        
        let contentInsets = UIEdgeInsetsMake(0, 0, -keyboard.height, 0)
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
        
        self.scrollView.isScrollEnabled = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    func ImageTapped(sender: AnyObject){
        print("Image tapped!")
        let image = UIImagePickerController()
        image.delegate = self
        image.allowsEditing = true
        
        present(image, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let image = info[UIImagePickerControllerOriginalImage] as? UIImage
        self.imgTeacher.image = image
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }

    func checkInfo() -> Bool{
        let ma = txtMa.text!
        if ma.isEmpty{
            alertWarnning(title: "Thông báo", message: "Ma giáo viên không được để trống")
            return false
        }
        //***** Comfirm email******
        if(isCheck != true){
            var students = [LibraryCard]()
            let fetchRequest:NSFetchRequest<LibraryCard> = LibraryCard.fetchRequest()
            fetchRequest.predicate = NSPredicate(format: "code == %@", ma)
            do{
                let searchResults = try Database.getContext().fetch(fetchRequest)
                students = searchResults
                if students.count > 0{
                    alertWarnning(title: "Thông báo", message: "Mã thẻ này đã tồn tại")
                    return false
                }
            }
            catch{
                print("Error: \(error)")
            }
        }
        //***************
        let name =  txtName.text!
        if name.isEmpty{
            alertWarnning(title: "Thông báo", message: "Tên không được để trống")
            return false
        }
        let deparment = txtKhoa.text!
        if deparment.isEmpty{
            alertWarnning(title: "Thông báo", message: "Khoa không được để trống")
            return false
        }
        return true
    }
    let teacherClassName:String = String(describing: LibraryCard.self)
    @IBAction func btnAddTeacher(_ sender: Any) {
        if(isCheck == true){
            let isSuccess = checkInfo()
            if isSuccess == false{
                return
            }
            teacher[pos].code = txtMa.text!
            teacher[pos].name = txtName.text!
            teacher[pos].department = txtKhoa.text!
            teacher[pos].birthday = dpkBirthDay.date as NSDate?
            teacher[pos].expirydate = dpkEndDay.date as NSDate?
            
            let imgData = UIImageJPEGRepresentation(imgTeacher.image!, 1)
            teacher[pos].image = imgData as NSData?
            Database.saveContext()
            txtMa.text = nil
            txtName.text = nil
            txtKhoa.text = nil
            _ = self.navigationController?.popViewController(animated: true)
            //performSegue(withIdentifier: "addEditTeacher_segue", sender: self)
        }else{
            let isSuccess = checkInfo()
            if isSuccess == false{
                return
            }
            let addTeacher:LibraryCard = NSEntityDescription.insertNewObject(forEntityName: teacherClassName, into: Database.getContext()) as! LibraryCard

            addTeacher.code = txtMa.text!
            addTeacher.name = txtName.text!
            addTeacher.department = txtKhoa.text!
            addTeacher.birthday = dpkBirthDay.date as NSDate?
            addTeacher.expirydate = dpkEndDay.date as NSDate?
            addTeacher.cardtype = "2"
        
            let imgData = UIImageJPEGRepresentation(imgTeacher.image!, 1)
            addTeacher.image = imgData as NSData?
            Database.saveContext()
            txtMa.text = nil
            txtName.text = nil
            txtKhoa.text = nil
            _ = self.navigationController?.popViewController(animated: true)
            //performSegue(withIdentifier: "detailAccount_segue", sender: self)
            //performSegue(withIdentifier: "addEditTeacher_segue", sender: self)
        }
        
    }
}
