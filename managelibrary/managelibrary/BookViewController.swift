//
//  BookViewController.swift
//  managelibrary
//
//  Created by Nguyen Do on 5/17/17.
//
//

import UIKit
import CoreData
enum selectedScopeBook:Int{
    case name = 0
    case author = 1
    case descript = 2
}
class BookViewController: UIViewController,UITableViewDataSource, UITableViewDelegate,UISearchBarDelegate{

    var bookInfo = BookInfo()
    @IBOutlet weak var tblBookTableView: UITableView!
    
    var majors: Majors!
    var majorsData = [Majors]()
    var book: [Book] = []
    var pos = -1
    override func viewDidLoad() {
        super.viewDidLoad()
        getMajors()
        SearchBarSettup()
        self.tblBookTableView.delegate = self
        self.tblBookTableView.dataSource = self
        book = majors.book!.allObjects as! [Book]
        tblBookTableView.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        book = majors.book!.allObjects as! [Book]
        self.tblBookTableView.reloadData()
    }
    
    //****** Search bar **********
    
    func SearchBarSettup(){
        let searchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 70))
        searchBar.showsScopeBar = true
        searchBar.scopeButtonTitles = ["Sách","Tác giả","Mô tả"]
        searchBar.selectedScopeButtonIndex = 0
        searchBar.delegate = self
        self.tblBookTableView.tableHeaderView = searchBar
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty {
            getMajors()
            book = majors.book!.allObjects as! [Book]
            self.tblBookTableView.reloadData()
        }else{
            filterTableView(ind: searchBar.selectedScopeButtonIndex, text: searchText)
        }
    }
    func filterTableView(ind: Int, text:String){
        switch ind {
        case selectedScopeBook.name.rawValue:
            book =  book.filter({ mod -> Bool in
                //return (mod.name?.lowercased().contains(text.lowercased()))!
                return (mod.name?.lowercased().localizedStandardContains(text.lowercased()))!
            })
            self.tblBookTableView.reloadData()
        case selectedScopeBook.author .rawValue:
            book =  book.filter({ mod -> Bool in
                //return (mod.author?.lowercased().contains(text.lowercased()))!
                return (mod.author?.lowercased().localizedStandardContains(text.lowercased()))!
            })
            self.tblBookTableView.reloadData()
        case selectedScopeBook.descript.rawValue:
            book =  book.filter({ mod -> Bool in
                //return (mod.descript?.lowercased().contains(text.lowercased()))!
                return (mod.descript?.lowercased().localizedStandardContains(text.lowercased()))!
            })
            self.tblBookTableView.reloadData()
        default:
            print("Không có dữ liệu")
        }
    }
    
    //******* End searchBar *********
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1 // display row
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return book.count// number row cell
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblBookTableView.dequeueReusableCell(withIdentifier: "BookCell", for: indexPath) as! BookTableViewCell
        let b = book[indexPath.row]
        cell.lblNameBook.text = b.name
        cell.lblAuthor.text = b.author
        cell.lblDescription.text! = b.descript!
        cell.lblBookShelf.text! = String(b.bookself)
        cell.lblNumberCount.text! = String(b.number)
        let image : UIImage = UIImage(data: b.image as! Data)!
        cell.imgImageBook.image = image
        cell.imgImageBook.layer.borderColor = UIColor.black.cgColor
        cell.imgImageBook.layer.cornerRadius = 50
        cell.imgImageBook.layer.masksToBounds = true
        cell.btnEditBook.tag = indexPath.row
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            Database.getContext().delete(book[indexPath.row])
            Database.saveContext()
            book.remove(at: indexPath.row)
            tblBookTableView.reloadData()
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        pos = indexPath.row
        performSegue(withIdentifier: "detailBook_Segue", sender: self)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "detailBook_Segue"{
            let vc = segue.destination as! DetailBookViewController
            vc.book = book[pos]
        }
        if segue.identifier == "editBook_Segue"{
            let vc = segue.destination as! UpdateBookViewController
            vc.book = book[pos]
            vc.majors = majorsData
        }
    }
    
    @IBAction func editBook(_ sender: AnyObject) {
        pos = sender.tag
    }
    
    func getMajors(){
        majorsData.removeAll()
        let fetchRequest:NSFetchRequest<Majors> = Majors.fetchRequest()
        do{
            let searchResults = try Database.getContext().fetch(fetchRequest)
            majorsData = searchResults
        }
        catch{
            print("Error: \(error)")
        }
    }
}
