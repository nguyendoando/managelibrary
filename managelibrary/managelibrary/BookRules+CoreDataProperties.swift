//
//  BookRules+CoreDataProperties.swift
//  managelibrary
//
//  Created by cao cuong on 6/4/17.
//
//

import Foundation
import CoreData


extension BookRules {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<BookRules> {
        return NSFetchRequest<BookRules>(entityName: "BookRules");
    }

    @NSManaged public var contents: String?

}
