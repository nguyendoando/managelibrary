//
//  ReturnBookTableViewController.swift
//  managelibrary
//
//  Created by cao cuong on 6/12/17.
//
//

import UIKit
import CoreData
enum selectedScope1:Int{
    case number = 0
}
class ReturnBookTableViewController: UITableViewController,UISearchBarDelegate {

    @IBOutlet weak var btnMenu: UIBarButtonItem!
    
    var detail = [BorrowedDetail]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        SearchBarSettup()

        if revealViewController() != nil {
            btnMenu.target = revealViewController()
            btnMenu.action = #selector(SWRevealViewController.revealToggle(_:))
        }
        getBorrowedDetails()
    }
    //search bar
    func SearchBarSettup(){
        let searchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 70))
        searchBar.showsScopeBar = true
        //searchBar.scopeButtonTitles = ["Tên","Mô tả"]
        searchBar.selectedScopeButtonIndex = 0
        searchBar.delegate = self
        self.tableView.tableHeaderView = searchBar
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty {
            getBorrowedDetails()
            self.tableView.reloadData()
        }else{
            filterTableView(ind: searchBar.selectedScopeButtonIndex, text: searchText)
        }
    }
    func filterTableView(ind: Int, text:String){
        switch ind {
        case selectedScope1.number.rawValue:
            detail = detail.filter({ mod -> Bool in
            return (mod.book?.name?.lowercased().localizedStandardContains(text.lowercased()))!
            })
            self.tableView.reloadData()
        default:
            print("Không có dữ liệu")
        }
    }
    //end search bar

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return detail.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ReturnBookCell", for: indexPath) as! ReturnBookTableViewCell

        // Configure the cell...
        let borrowedDetail = detail[indexPath.row]
        let card = borrowedDetail.card! as LibraryCard
        cell.lblCardCode.text = card.code
        let book = borrowedDetail.book! as Book
        cell.lblBookName.text = book.name
        cell.lblRentedNumber.text = String(borrowedDetail.number)
        
        cell.btnReturnBook.tag = indexPath.row

        return cell
    }
    
   /* @IBAction func returnBook(_ sender: AnyObject) {

        let row = sender.tag
        let borrowedDetail = detail[row!]
        let book = borrowedDetail.book! as Book
        book.restnumber += borrowedDetail.number
        Database.getContext().delete(borrowedDetail)
        Database.saveContext()
        detail.remove(at: row!)
        tableView.reloadData()
    } */
    //****************
    
    func okOrCancel(row: Int){
        let refreshAlert = UIAlertController(title: "Thông báo", message: "Bạn có chắc muốn trả sách.", preferredStyle: UIAlertControllerStyle.alert)
        
        refreshAlert.addAction(UIAlertAction(title: "Ðồng ý", style: .default, handler: { (action: UIAlertAction!) in
            let borrowedDetail = self.detail[row]
            let book = borrowedDetail.book! as Book
            book.restnumber += borrowedDetail.number
            Database.getContext().delete(borrowedDetail)
            Database.saveContext()
            self.detail.remove(at: row)
            self.tableView.reloadData()
        }))
        
        refreshAlert.addAction(UIAlertAction(title: "Hủy", style: .default, handler: { (action: UIAlertAction!) in
            
        }))
        
        present(refreshAlert, animated: true, completion: nil)
    }
    
    @IBAction func returnBook(_ sender: AnyObject) {
        let row = sender.tag
        okOrCancel(row: row!)
    }
    //***************	


    func getBorrowedDetails(){
        detail.removeAll()
        let fetchRequest:NSFetchRequest<BorrowedDetail> = BorrowedDetail.fetchRequest()
        do{
            let searchResults = try Database.getContext().fetch(fetchRequest)
            detail = searchResults
        }
        catch{
            print("Error: \(error)")
        }
    }

}
