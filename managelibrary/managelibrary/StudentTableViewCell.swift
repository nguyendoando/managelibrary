//
//  StudentTableViewCell.swift
//  managelibrary
//
//  Created by cao cuong on 5/30/17.
//
//

import UIKit

class StudentTableViewCell: UITableViewCell {

    @IBOutlet weak var imgStudent: UIImageView!
    @IBOutlet weak var lblCode: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblClass: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
